from django.contrib import admin
from app0compound.models import CompoundBasicLevel, CompoundDetailLevel

# Register your models here.
#admin.site.register( CompoundBasicLevel )
class CompoundBasicLevelAdmin( admin.ModelAdmin ): 
    list_display = ("compound","compound_lincs_name","compound_lincs_identifier")
    search_fields = ("compound","compound_lincs_name","compound_lincs_identifier")
admin.site.register( CompoundBasicLevel, CompoundBasicLevelAdmin )

#admin.site.register( CompoundDetailLevel )
class CompoundDetailLevelAdmin( admin.ModelAdmin ): 
    list_display = ("compound_basiclevel","compound_detaillevel_serial","compound_provider","compound_provider_catalog_id","compound_provider_batch_id","compound_purity","compound_stocksolution_concentration_unit","compound_stocksolution_concentration_value","compound_dilution_factor_denominator","compound_refernce_url","compound_notes")
    search_fields = ("compound_basiclevel","compound_detaillevel_serial","compound_provider","compound_provider_catalog_id","compound_provider_batch_id","compound_stocksolution_concentration_unit","compound_stocksolution_buffer","compound_dilution_buffer","compound_notes")
    filter_horizontal = ("compound_stocksolution_buffer","compound_dilution_buffer")
admin.site.register( CompoundDetailLevel, CompoundDetailLevelAdmin )
