from django.db import models

# ontologies
from apponcompound_bioontology.models import Compound
from apponprovider_own.models import Provider
from apponunit_bioontology.models import Unit

models# bue 20150211: evt I moved the basic level into the detailed level. yet the basic level would only contain: compound, compound_lincs_name, compound_lincs_identifier
class CompoundBasicLevel( models.Model ):
    compound = models.ForeignKey( Compound, primary_key=True, default="not_yet_specified", verbose_name="Compound", help_text="Choose compound." )
    compound_lincs_name = models.CharField( max_length=255, verbose_name="Lincs sample name", help_text="Official lincs small molecule name for lincs project related small molecule.", blank=True )
    compound_lincs_identifier = models.CharField( max_length=255, verbose_name="Lincs sample identifier", help_text="Official lincs small molecule identifier for lincs prj related small molecule.", blank=True)
    def __str__( self ):
        return( self.compound.annot_id )

    class Meta:
        ordering = ["compound"]


class CompoundDetailLevelManager( models.Model ):
    def get_by_natural_key( self, compound_basiclevel, compound_detaillevel_serial ):
        return self.get( compound_basiclevel=compound_basiclevel, compound_detaillevel_serial=compound_detaillevel_serial )

class CompoundDetailLevel( models.Model ):
    objects = CompoundDetailLevelManager()    
    compound_basiclevel = models.ForeignKey( CompoundBasicLevel, default="not_yet_specified", verbose_name="Compound", help_text="Choose compound." )
    compound_detaillevel_serial = models.SlugField( default="not_yet_specified", verbose_name="Sample provider level serial number", help_text="Uniqe serial number for this sample. Only alphanumeric keys and underscore allowed." )
    compound_provider = models.ForeignKey( Provider, default="not_yet_specified", verbose_name="Provider", help_text="Choose provider." )
    compound_provider_catalog_id = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Provider catalog id", help_text="ID or catalogue number or name assigned to the compound by the vendor or provider." )
    compound_provider_batch_id = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Provider batch id", help_text="Batch or lot number assigned to the compound by the vendor or provider." )
    compound_purity = models.FloatField( default=None, verbose_name="Purity", help_text="Purity percentage.", null=True, blank=True )
    compound_stocksolution_concentration_unit = models.ForeignKey( Unit, default="not_yet_specified", verbose_name="Stock solution concentration unit", help_text="Value unit." )
    compound_stocksolution_concentration_value = models.FloatField( default=None, verbose_name="Stock solution concentration value", help_text="Value.", null=True, blank=True )
    compound_stocksolution_buffer = models.ManyToManyField( Compound, related_name="CompoundStocksolutionBuffer", verbose_name="Stock solution buffer compounds", help_text="Choose all utilised stock buffer compounds" )
    compound_dilution_factor_denominator = models.PositiveIntegerField( default=None, verbose_name="Dilution factor 1/n", help_text="Diltuion factor denominator n.", null=True, blank=True )
    compound_dilution_buffer = models.ManyToManyField( Compound, related_name="CompoundDilutionBuffer", verbose_name="Dilution buffer compounds", help_text="Choose all utilised dilutant compounds" )
    compound_refernce_url = models.URLField( default="https://not.yet.specified", verbose_name="Compound reference URL", help_text="URL to the vendor or provider secific compound specification." ) 
    compound_notes = models.TextField( verbose_name="Notes", help_text="Additional this compound related notes", blank=True )  # ph, stock concentration value, stock storage temperature
    def __str__( self ):
        s_out = self.compound_basiclevel.compound.annot_id+'-'+self.compound_detaillevel_serial
        return( s_out )
    def natural_key( self ):
        return( self.compound_basiclevel.pk, self.compound_detaillevel_serial  )
    natural_key.dependencies = ["app0compound.CompoundBasicLevel"]

    class Meta:
        unique_together = (("compound_basiclevel","compound_detaillevel_serial"),)
        ordering =["compound_basiclevel","compound_detaillevel_serial"]
