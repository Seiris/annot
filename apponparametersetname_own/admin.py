from django.contrib import admin
from apponparametersetname_own.models import ParameterSetName


# Register your models here.
#admin.site.register( ParameterSetName )
class ParameterSetNameAdmin( admin.ModelAdmin ):
    list_display = ('term_name','annot_id','term_source_version_responsible','term_source_version_update','term_source_version','term_id','term_ok') 
    readonly_fields = ('term_source_version_update','term_source_version','term_id','term_ok',) 
    search_fields = ('term_name','term_id','annot_id') 

# register
admin.site.register( ParameterSetName, ParameterSetNameAdmin ) 
