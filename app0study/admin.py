from django.contrib import admin
## bridges
from app0study.models import Study, ZoomStudyPerson, ZoomStudyPublication
## study assay bridges here 
#from appas_measurement_technology_platform.models import StudyAssayMeasureTechPlatform
#from appas_drugscreen.models import StudyAssayDrugscreen
from appas_mema.models import ZoomStudyAssayMema
#from appas_rnaseq.models import StudyAssayRnaseq  # transcription_profiling nucleotide_sequencing illumina


# Register your models here.
## bridge study assays here 
class ZoomStudyAssayMemaInline(admin.StackedInline):
    model = ZoomStudyAssayMema
    extra = 0


## bridges 
# bridge person
#admin.site.register( ZoomStudyPerson )
class ZoomStudyPersonAdmin(  admin.ModelAdmin ):
    list_display = ("study","person")
    search_fields = ("study","person","study_person_role")
    filter_horizontal = ("study_person_role", )
admin.site.register( ZoomStudyPerson, ZoomStudyPersonAdmin )

class ZoomStudyPersonInline(admin.StackedInline):
    model = ZoomStudyPerson
    filter_horizontal = ("study_person_role", )
    extra = 0

# bridge publication 
#admin.site.register( ZoomStudyPublication )
class ZoomStudyPublicationAdmin(  admin.ModelAdmin ):
    list_display = ("study","publication")
    search_fields = ("study","publication")
admin.site.register( ZoomStudyPublication, ZoomStudyPublicationAdmin) 

class ZoomStudyPublicationInline(admin.StackedInline):
    model = ZoomStudyPublication
    extra = 0

## study 
#admin.site.register( Study )
class StudyAdmin( admin.ModelAdmin ): 
    list_display = ("s_identifier","s_title","s_description","s_submission_date","s_public_release_date")
    search_fields = ("s_identifier","s_title","s_description")
    #inlines = [ZoomStudyPublicationInline, ZoomStudyPersonInline ]
    inlines = [ZoomStudyPublicationInline, ZoomStudyPersonInline, ZoomStudyAssayMemaInline]
admin.site.register( Study, StudyAdmin )
