from django.db import models
# ontologies
from apponexperimentaldesigntype_bioontology.models import ExperimentalDesignType
from apponpersonrole_own.models import PersonRole
# bridges 
from app0person.models import Person
from app0publication.models import Publication

# Create your models here.
class Study( models.Model ):
    # STUDY
    s_identifier = models.SlugField( primary_key=True, default="not_yet_specified", verbose_name="Study identifier", help_text="A unique identifier." ) 
    s_title = models.CharField( default="not_yet_specified", verbose_name="Study title", max_length=255, help_text="A concise phrase used to encapsulate the purpose and goal of the study." )
    s_description = models.TextField( default="not_yet_specified", verbose_name="Study description", help_text="A textual description of the study, with components such as objective or goals." ) 
    s_submission_date = models.DateField( default="1955-11-05", verbose_name="Study submission date", help_text="The date on which the study is submitted to an archive." )
    s_public_release_date = models.DateField( default="1955-11-05", verbose_name="Study public release date", help_text="The date on which the study should be released publicly." )
    #s_file_name = ( "Study File Name", help_text="A field to specify the name of the Study file corresponding the definition of that Study. There can be only one file per cell." )

    # STUDY DESIGN DESCRIPTORS 
    s_designe_type = models.ForeignKey( ExperimentalDesignType, default="not_yet_specified", verbose_name="Experimental design type", help_text="A term allowing the classification of the study based on the overall experimental design, e.g cross- over design or parallel group design." )

    # STUDY ASSAYS (inclusive FACTORS, PARAMETERS, PROTOCOLS, DATATRANSFORMATIONS, NORMALIZATIONS)  
    #s_assay_file_name = ( "Study Assay File Name", help_text="A field to specify the name of the Assay file corresponding the definition of that assay. There can be only one file per cell." )
    # goto admin and app bra measurement_technology_platform bridges

    # STUDY PUBLICATIONS
    # goto admin and app brs publication bridge

    # STUDY CONTACTS
    # goto admin and app brs person bridge

    def __str__( self ):
        return ( self.s_identifier)

    class Meta:
        ordering = ["s_identifier"]



# study publication bridge 
class ZoomStudyPublicationManager( models.Manager ): 
    def get_by_natural_key( self, study, publication ):
        return self.get( study=study, publication=publication )

class ZoomStudyPublication( models.Model ): 
    objects = ZoomStudyPublicationManager()
    study = models.ForeignKey ( Study, default="not_yet_specified", help_text="Study." )
    publication = models.ForeignKey( Publication, default="not_yet_specified", help_text="Publication associated with this study." ) 
    def __str__( self ):
        s_out = self.study.s_identifier+'-'+self.publication.publication_title
        return( s_out )
    def natural_key( self ):
        return( self.study.pk, self.publication.pk )
    natural_key.dependencies=["app0study.Study","app0publication.Publication"]
  
    class Meta: 
        unique_together = (("study","publication"),)
        ordering = ["study","publication"]
        

# study person bridge 
class ZoomStudyPersonManager( models.Manager ):
    def get_by_natural_key( self, study, person ): 
        return self.get( study=study, person=person )

class ZoomStudyPerson( models.Model ): 
    objects = ZoomStudyPersonManager()
    study = models.ForeignKey ( Study, default="not_yet_specified", help_text="Study." )
    person = models.ForeignKey( Person, help_text="Person associated with the study." ) 
    study_person_role = models.ManyToManyField( PersonRole, help_text="Term to classify the role(s) performed by this person in the context of the study, which means that the roles reported here need not correspond to roles held withing their affiliated organization. Multiple annotations or values attached to one person can be provided." ) 
    def __str__( self ):
        s_out = self.study.s_identifier+'-'+self.person.person_first_name+'_'+self.person.person_mid_initials+'_'+self.person.person_last_name
        return( s_out )
    def natural_key( self ): 
        return( self.study.pk, self.person.pk )
    natural_key.dependencies=["app0study.Study","app0person.Person"]

    class Meta: 
        unique_together = (("study","person"),)
        ordering = ["study","person"]
