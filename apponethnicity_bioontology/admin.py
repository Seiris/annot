from django.contrib import admin
from apponethnicity_bioontology.models import Ethnicity


# Register your models here.
#admin.site.register( Ethnicity )
class EthnicityAdmin( admin.ModelAdmin ):
    list_display = ('term_name','annot_id','term_source_version_responsible','term_source_version_update','term_source_version','term_id','term_ok') 
    readonly_fields = ('term_source_version_update','term_source_version','term_id','term_ok',) 
    search_fields = ('term_name','term_id','annot_id') 

# register
admin.site.register( Ethnicity, EthnicityAdmin ) 
