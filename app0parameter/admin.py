from django.contrib import admin
from app0parameter.models import ParameterSingle, ParameterSet


# Register your models here.

# parameter
#admin.site.register( ParameterSingle )
class ParameterSingleAdmin(  admin.ModelAdmin ):
    list_display = ("parameter","reagent_aliquot_serial","reagent_concentration_unit","reagent_concentration_value")
    search_fields = ("parameter","reagent_aliquot_serial","reagent_concentration_unit","reagent_concentration_value")
admin.site.register( ParameterSingle, ParameterSingleAdmin )


# paramaeter set 
#admin.site.register( ParameterSet )
class ParameterSetAdmin( admin.ModelAdmin ):
    list_display = ("parameterset_name",)
    search_fields = ("parameterset_name","parameter")
    filter_horizontal = ("parameter",)
admin.site.register( ParameterSet, ParameterSetAdmin )
