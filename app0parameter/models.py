from django.db import models
# import ontologies
from apponunit_bioontology.models import Unit
from apponparametersetname_own.models import ParameterSetName
# import bridges
from appsabrick.models import Reagent

# Create your models here.
# parameter
class ParameterSingleManager( models.Manager ):
    def get_by_natural_key(self, parameter__reagent_type, parameter__reagent_name, reagent_aliquot_serial, reagent_concentration_unit, reagent_concentration_value ):
        return self.get( parameter__reagent_type=parameter__reagent_type, parameter__reagent_name=parameter__reagent_name, reagent_aliquot_serial=reagent_aliquot_serial, reagent_concentration_unit=reagent_concentration_unit, reagent_concentration_value=reagent_concentration_value )
 
class ParameterSingle( models.Model ):
    objects = ParameterSingleManager() 
    parameter = models.ForeignKey( Reagent, help_text="Reagent becomming parameter." )
    reagent_aliquot_serial = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Lab level aliquot identification", help_text="Write down the aliquot identifier. Only alphanumeric keys and underscore allowed." )
    reagent_concentration_unit = models.ForeignKey( Unit, default="not_yet_specified", help_text="Value unit." )
    reagent_concentration_value = models.FloatField( default=None, help_text="Value.", null=True, blank=True )
    def __str__( self ):
        s_out = self.parameter.reagent_type+'|'+self.parameter.reagent_name+'-'+self.reagent_aliquot_serial+'|'+self.reagent_concentration_unit.annot_id+'-'+str(self.reagent_concentration_value)
        return( s_out)
    def natural_key( self ):
        return( self.parameter.natural_key()+(self.reagent_aliquot_serial, self.reagent_concentration_unit.pk, self.reagent_concentration_value,) )
    natural_key.dependencies = ["appsabrick.Reagent","apponunit_bioontology.Unit"]

    class Meta: 
        unique_together = (("parameter", "reagent_aliquot_serial", "reagent_concentration_unit","reagent_concentration_value"),)
        ordering = ["parameter","reagent_aliquot_serial","reagent_concentration_unit","reagent_concentration_value"]

# parameter set 
# parameter set track concentration and aliquote in assay separartely e.g. in mema as barecode and layoufile info 
class ParameterSet( models.Model ):
    parameterset_name = models.ForeignKey( ParameterSetName, primary_key=True, default="not_yet_specified", help_text="Set a parameterset name." )
    parameter = models.ManyToManyField( Reagent, help_text="Choose reagents which are  becomm parameters." )    
    def __str__( self ):
        return( self.parameterset_name.annot_id )

    class Meta:
        ordering = ["parameterset_name"]
