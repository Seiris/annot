from django.contrib import admin
from app0protocol.models import ProtocolBasicLevel, ProtocolDetailLevel, ProtocolComponentBasicLevel, ProtocolComponentDetailLevel, ProtocolRef, ResultFile, ResultUrl, ResultProtocolRef

# Register your models here.
#admin.site.register( ProtocolBasicLevel )
class ProtocolBasicLevelAdmin( admin.ModelAdmin ):
    list_display = ( "protocol_type","protocol_name" )
    search_fields = ( "protocol_type","protocol_name" )
admin.site.register( ProtocolBasicLevel, ProtocolBasicLevelAdmin )

#admin.site.register( ProtocolDetailLevel )
class ProtocolDetailLevelAdmin( admin.ModelAdmin ):
    list_display = ( "protocol_basiclevel","protocol_version","protocol_description","protocol_url" )
    search_fields = ( "protocol_basiclevel","protocol_version","protocol_description" )
admin.site.register( ProtocolDetailLevel, ProtocolDetailLevelAdmin )


# result component bridge
#admin.site.register( ProtocolComponentBasicLevel )
class ProtocolComponentBasicLevelAdmin( admin.ModelAdmin ): 
    list_display = ( "protocol_component_type","protocol_component_name" )
    search_fields = ( "protocol_component_type","protocol_component_name" )
admin.site.register( ProtocolComponentBasicLevel, ProtocolComponentBasicLevelAdmin )

#admin.site.register( ProtocolComponentDetailLevel )
class ProtocolComponentDetailLevelAdmin( admin.ModelAdmin ): 
    list_display = ( "protocol_basiclevel", )
    search_fields = ( "protocol_basiclevel","protocol_component_basiclevel" )
    filter_horizontal = ("protocol_component_basiclevel",)
admin.site.register( ProtocolComponentDetailLevel, ProtocolComponentDetailLevelAdmin )


#admin.site.register( ProtocolRef )
class ProtocolRefAdmin( admin.ModelAdmin): 
    list_display = ( "protocol_detaillevel","performer","dday" ) 
    search_fields = ( "protocol_detaillevel","performer","dday" )  
admin.site.register( ProtocolRef, ProtocolRefAdmin )  


# result protocol ref bridge
# result file
#admin.site.register( ResultFile )
class ResultFileAdmin( admin.ModelAdmin ): 
    list_display = ( "result_filename","result_file" )
    search_fields = ( "result_filename","result_file", )
admin.site.register( ResultFile, ResultFileAdmin )

# result url
#admin.site.register( ResultUrl )
class ResultUrlAdmin( admin.ModelAdmin ): 
    list_display = ( "result_url", )
    search_fields = ( "result_url", )
admin.site.register( ResultUrl, ResultUrlAdmin )


#admin.site.register( ResultProtocolRef ) 
class ResultProtocolRefAdmin(  admin.ModelAdmin ):
    list_display = ( "result_version_ok","result_version","protocol_ref" )
    search_fields = ( "result_version_ok","result_version","protocol_ref","result_file","result_url" )
    filter_vertical = ( "result_file","result_url" )
admin.site.register( ResultProtocolRef, ResultProtocolRefAdmin )
