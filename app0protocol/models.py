from django.db import models
# ontologies 
from apponprotocoltype_own.models import ProtocolType
from apponprotocolcomponenttype_own.models import ProtocolComponentType
# bridges
from app0person.models import Person


# Create your models here.

# protocol 
# bue 20141119 : all protocols will be stored online.
# bue 20141119 : placing of parametes take care in the correspoding appasassays.  

class ProtocolBasicLevel( models.Model ):
    protocol_type = models.ForeignKey( ProtocolType, default="not_yet_specified", help_text="Term to classify the protocol." )
    protocol_name = models.SlugField( primary_key=True, default="not_yet_specified", help_text="The name of the protocols." )
    # protocol_component 
    # parameter
    def __str__( self ):
        return( self.protocol_name )

    class Meta:
        ordering = ["protocol_type","protocol_name"]


class ProtocolDetailLevelManager( models.Manager ):
    def get_by_natural_key(self, protocol_basiclevel, protocol_version ):
        return self.get( protocol_basiclevel=protocol_basiclevel, protocol_version=protocol_version )

class ProtocolDetailLevel( models.Model ):
    objects = ProtocolDetailLevelManager()
    protocol_basiclevel = models.ForeignKey( ProtocolBasicLevel, default="not_yet_specified", verbose_name="Protocol name", help_text="Choose protocol." )
    protocol_version = models.SlugField( default="not_yet_specified", help_text="An identifier for the version to ensure protocol tracking." )
    protocol_description = models.TextField( default="not_yet_specified", help_text="Free-text description of the protocol." )
    protocol_url = models.URLField( default="https://not.yet.specified", help_text="Pointer to protocol resources external to the ISA-TAB that can be accessed by their Uniform Resource Location (URL)." )
    # parameter
    def __str__( self ):
        s_out = self.protocol_basiclevel.protocol_name+'-'+self.protocol_version
        return( s_out )
    def natural_key( self ): 
        return( self.protocol_basiclevel.pk, self.protocol_version )
    natural_key.dependencies = ["app0protocol.ProtocolBasicLevel"]

    class Meta: 
        unique_together = (("protocol_basiclevel","protocol_version"),)
        ordering = ["protocol_basiclevel","protocol_version"]


# bue 20150227: protocol component bridge is  linked on a basiclevel. 
#  protocol version related components would not be supported. if something like this would occure, consider to setup a entier new protocol, starting at the basic level. 
class ProtocolComponentBasicLevel( models.Model ):
    protocol_component_type = models.ForeignKey( ProtocolComponentType, default="not_yet_specified", help_text="Term to classify the protocol components listed for example, instrument, software, detector or reagent." )
    protocol_component_name = models.SlugField( primary_key=True, default="not_yet_specified", help_text="Protocol component e.g. instrument names, software names, and reagents names." )
    #protocol_component_name = models.ForeignKey( ProtocolComponentName, help_text="Protocol component e.g. instrument names, software names, and reagents names." )
    def __str__( self ): 
        return( self.protocol_component_name )

    class Meta:
        ordering = ["protocol_component_type","protocol_component_name"]


class ProtocolComponentDetailLevel( models.Model ): 
    protocol_basiclevel = models.ForeignKey( ProtocolBasicLevel, primary_key=True, verbose_name="Protocol name", default="not_yet_specified", help_text="Related protocol." )
    protocol_component_basiclevel = models.ManyToManyField( ProtocolComponentBasicLevel, verbose_name="Protocol component name", help_text="Related protocol compoound." )
    def __str__( self ): 
        #s_out = self.protocol_basiclevel.protocol_name  # str(len(self.protocol_component_basiclevel))
        return( self.protocol_basiclevel.protocol_name )
    
    class Meta: 
        ordering = ["protocol_basiclevel"]


# protocol assay bridge 
# bue 20141201: evt no own primary key construct, multiple time execution of the same protocol by the same person on the same day is possible. or replace date by datetime. 
class ProtocolRefManager( models.Model):
    def get_by_natural_key( self, protocol_detaillevel__protocol_basiclevel, protocol_detaillevel__protocol_version, performer, dday ):
        return self.get( protocol_detaillevel__protocol_basiclevel=protocol_detaillevel__protocol_basiclevel, protocol_detaillevel__protocol_version=protocol_detaillevel__protocol_version, performer=performer, dday=dday )

class ProtocolRef( models.Model ): 
    obj = ProtocolRefManager()
    protocol_detaillevel = models.ForeignKey( ProtocolDetailLevel, verbose_name="Protocol", help_text="Protocol associated with this study." )  # bue 20150225: sorry with natural keys i dont get a proper default running  
    performer = models.ForeignKey( Person, help_text="Most probably you." )  # bue 20150225: sorry with natural keys i dont get a proper default running
    dday = models.DateField(  default="1955-11-05", verbose_name="Protocol execution start date", help_text="The date on which the protocol execution begun." )
    #dday = models.PositiveIntegerField( default=19551105, verbose_name="Protocol execution start date as positive integer", help_text="The date on which the protocol execution begun." )
    def __str__( self ):
        s_out = self.protocol_detaillevel.protocol_basiclevel.protocol_name+'-'+self.performer.person_identifier+'-'+str(self.dday).replace('-','')
        return( s_out )
    def natural_key( self ):
        return( self.protocol_detaillevel.natural_key()+(self.performer.pk,self.dday) )
    natural_key.dependencies = ["app0protocol.ProtocolDetailLevel","app0person.Person"]

    class Meta: 
        unique_together = (("protocol_detaillevel", "performer", "dday"),)
        ordering = ["protocol_detaillevel","performer","dday"]

        
# result protocol file 
class ResultFile( models.Model ):
    result_filename = models.CharField( max_length=255, primary_key=True, help_text="Unique result protocol filename." )
    result_file = models.FileField( help_text="Upload result protocol related result file." )
    def __str__( self ):
        return( self.result_filename )
    
    class Meta: 
        ordering = ["result_filename","result_file"]


# result protocol url
class ResultUrl( models.Model ): 
    result_url = models.URLField( primary_key=True, default="https://not.yet.specified", help_text="Link to result protocol related result URL." ) 
    def __str__( self ):
        return( self.result_url )
    
    class Meta: 
        ordering = ["result_url"]


# result protocol
class ResultProtocolRefManager( models.Model ):
    def get_by_natural_key( self, protocol_ref__protocol_detaillevel__protocol_basiclevel, protocol_ref__protocol_detaillevel__protocol_version, protocol_ref__performer, protocol_ref__dday, result_version ): 
        return self.get( protocol_ref__protocol_detaillevel__protocol_basiclevel=protocol_ref__protocol_detaillevel__protocol_basiclevel, protocol_ref__protocol_detaillevel__protocol_version=protocol_ref__protocol_detaillevel__protocol_version, protocol_ref__performer=protocol_ref__performer, protocol_ref__dday=protocol_ref__dday, result_version=result_version )

class ResultProtocolRef( models.Model ): 
    objects = ResultProtocolRefManager()
    result_version_ok = models.BooleanField( default=False, help_text="Is this the most reliable result version?") 
    result_version = models.PositiveIntegerField( default=None, help_text="Integer.", null=True, blank=True ) 
    protocol_ref = models.ForeignKey( ProtocolRef, help_text="Protocol refernce associated with this results." )
    readme = models.TextField( default="not_yet_specified", help_text="Result files related comment." )
    result_file = models.ManyToManyField( ResultFile, help_text="Choose result protocol related result file." )
    result_url = models.ManyToManyField( ResultUrl, help_text="Choose result protocol related result url" )
    def __str__( self ):
        s_out = self.protocol_ref.protocol_detaillevel.protocol_basiclevel.protocol_name+'-'+self.protocol_ref.protocol_detaillevel.protocol_version+'-'+self.protocol_ref.performer.person_identifier+'-'+str(self.protocol_ref.dday).replace('-','')+'-'+str(self.result_version)
        return( s_out )
    def natural_key( self ):
        return( self.protocol_ref.natural_key()+(self.result_version,) )
    natural_key.dependencies = ["app0protocol.ProtocolRef"]

    class Meta: 
        unique_together = (("protocol_ref","result_version"),)
        ordering = ["protocol_ref","result_version"]
