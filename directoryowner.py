import os
import glob

ls_app = glob.glob("app*")  # "app0*", "appon*", "appas_*"
print( ls_app )
#s_app = "appp"
for s_app in ls_app: 
    print( "Running :", s_app )
    # mkdir
    #s_command = "mkdir "+s_app+"/fixtures"
    #os.system( s_command )
    s_command = "mkdir "+s_app+"/migrations"
    os.system( s_command )

    # touch seed
    #s_command = "touch "+s_app+"/fixtures/seed"
    #os.system( s_command )
    s_command = "touch "+s_app+"/migrations/seed"
    os.system( s_command )

    # chmod
    s_command = "chmod -R 775 "+s_app+"/fixtures"
    os.system( s_command )

    # chown
    s_command = "chown -R bue:www "+s_app+"/fixtures"
    os.system( s_command )

    # git add 
    #s_command = "git add "+s_app+"/fixtures"
    #os.system( s_command )

print( "ok" )
