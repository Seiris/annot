User Manual
===========

This manual is intended for the user of Annot.
First this manual will cover how-to install and run Annot for your lab or you self.
Secound this manual will describe all graphical user interface (GUI) features implemented. 

How-to Get Annot Up and Running
-------------------------------

Install Annot
^^^^^^^^^^^^^
1. `Install Docker <https://docs.docker.com/>`_ on the machine you would like to run Annot for you lab.
    Docker is able runs on Linux, Mac OSX, MS Windos and many flavours of cloud platforms.
   
2. Fetch the latest annot related Docker images, hosted at `Docker hub <https://registry.hub.docker.com/search?>`_. Run on the command line: ::

    docker pull elmar/media
    docker pull elmar/annot
    docker pull elmar/apache_annot
    
3. Generate a `bioontology.org BioPortal <http://bioportal.bioontology.org/login?>`_ account.
   Go to you account settings to figure out your application interface (API) key.
   

4. Write a plain text crowbar.py addaption with the following content: ::
     
    SECRET_KEY = "about_64_characters_long`~!@#$%^&*()-+=:;<,>.?/"
    DATABASE_PASSWORD = "intended_secret_annot_postgresql_database_password"
    APIKEY = "your_BioPortal_bioontology.org_API_key"
    CONTACT = "you@emailadress"
    
5. Build the Docker container out of the fetched images. Addapt /path/to/crowbar.py in the last docker run command below to your settings. If you have to choose an other port then 80, choose a `port <https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers>`_ between 49152 and 65535. For the rest of this documentation we assume Annot to run on port 52872, as specified by the last docker run command.::

    docker run --name=media elmar/media /bin/true
    docker run --name=annot elmar/annot /bin/true
    docker run --name=apache_annot --volumes-from=media --volumes-from=annot -v=/path/to/crowbar.py:/var/www/annot/prjannot/crowbar.py -d -p 52872:80 elmar/apache_annot

.. _link_developmer_manual:

.. bue 20150512: add .bashrc file to apache_annot root whith content
   export PYTHONPATH=/var/www/annot:$PYTHONPATH

6. Figure out the hostname or IP address from the machine you are running docker.
   Check out the `Docker installation documentation <https://docs.docker.com/>`_ for detailed informaton how to do this.
   For the rest of the documentation we assume IP address 127.0.0.1 .
   
7. Enter apache_annot. Run on the command line: ::
     
    docker exec -ti apache_annot /bin/bash
    
  a. Indside apache_annot check if the postgresql database daemon is running. Start if necesary. ::
     
      /etc/init.d/postgresql status
      /etc/init.d/postgresql start

  b. Indside apache_annot create Postgresql annot user and annot database. Execute the commands one by one! Answer the posed questions. For the annot user utilize same password as specified in the cowbar.py file before. ::
     
      su postgres
      createdb annot
      createuser -P annot
      psql -U postgres -d annot -c"GRANT ALL PRIVILEGES ON DATABASE annot TO annot;"
      exit

  c. Inside apache_annot set up the annot project. Execute the commands one by one! ::

      chown www-data:root -R /var/www/media
      cd /var/www/annot/
      python3 demigrations.py
      python3 manage.py makemigrations
      python3 manage.py migrate
      python3 manage.py createsuperuser
      python3 manage.py collectstatic
      exit

8. Fire up you favourite web browser - we recommend mozilla's `Firefox <https://www.mozilla.org/>`_ - and surf to the place where you Annot is running. In this documentation we assume Annot is running in IP address 127.0.0.1 port 52872. Write into the browsers addressbar: ::
     
    http://127.0.0.1:52872/admin/

9. Log in with the above generated superuser credential.

That's all.


Run Annot
^^^^^^^^^

Backup Annot
^^^^^^^^^^^^ 


User Interface
--------------

Investigation and Study Metadata
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: app0investigation
.. automodule:: app0study

Person and Publication Metadata
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: app0person
.. automodule:: app0publication

		
Assay Workflows
^^^^^^^^^^^^^^^
.. automodule:: appas_mema

Assay Workflow Building Bricks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: app0sample
.. automodule:: app0factor
.. automodule:: app0parameter
.. automodule:: app0protocol

Sample and Reagent Metadata
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. app0sample_viruses
.. app0sample_bacteria
.. app0sample_archea
.. app0sample_eukaryota
.. app0sample_plantae
.. app0sample_fungi
.. app0sample_animalia
.. app0sample_human
.. automodule:: app0antibody
.. automodule:: app0compound
.. automodule:: app0protein

Controled Vocabulary
^^^^^^^^^^^^^^^^^^^^
.. appon*

System Administration
^^^^^^^^^^^^^^^^^^^^^
.. automodule:: appsabrick
.. automodule:: appsavocabulary
.. appsavocabulary.ich.du
