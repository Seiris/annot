Developer Manual
================

This manual is intended for programers who would like to take part in the Annot project code development or adapt the Annot source code to their needs.

You can skip step 4 and git clone git\@gitlab.com:biotransistor/annot.git without forking if you only want to adapt the Annot source code to your needs and do not plan to fork your changes back to the Annot project master branch.


How-to Get Annot Developer Version Up and Running
-------------------------------------------------

Install Annot for Development
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. `Install Docker <https://docs.docker.com/>`_ on the computer you would like to run Annot for development. (This can be the same at the computer where you run your deployed version of Annot.)
    Docker is able run on Linux, Mac OSX, MS Windows and many flavours of cloud platforms.

2. Fetch the latest Annot related Docker images, hosted at `Docker hub <https://registry.hub.docker.com/search?>`_. Run on the command line: ::

    docker pull elmar/media
    docker pull elmar/apache_annot
    
3. On the development computer install `Git <http://git-scm.com/>`_. This will depend on the flavour of the operating system you are running.

   
.. bue 20150511: I have to start to not directly work in the master branch!

4. Get the Annot source code
   
  a. Sign in at `Gitlab <https://about.gitlab.com/>`_, sign up if you do not already have an account.
   
  b. Fork from https://gitlab.com/biotransistor/annot the master branch.
   
  c. Clone from your fork. In the example below replace your_namespace with your gitlab username. This example would clone from the master branch of your fork. ::
   
      git clone git@gitlab.com:your_namespace/annot.git

      
5. Generate a `bioontology.org BioPortal account <http://bioportal.bioontology.org/login?>`_. Go to you account settings to figure out your application interface (API) key.

6. Write a plain text crowbar.py adaption with the following content: ::

    SECRET_KEY = "about_64_characters_long`~!@#$%^&*()-+=:;<,>.?/"
    DATABASE_PASSWORD = "intended_secret_annot_postgresql_database_password"
    APIKEY = "your_BioPortal_bioontology.org_API_key"
    CONTACT = "you@emailadress"

7. Build the Docker container out of the fetched images and the cloned source code. Adapt  /path/to/annot and /path/to/crowbar.py in the last docker run command below to your settings. If you have to choose an other port then 80, choose a `port <https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers>`_ between 49152 and 65535. For the rest of this documentation we will assume Annot is running on port 35627, as specified by the last docker run command.::

    docker run --name=media elmar/media /bin/true
    docker run --name=apache_annotdev --volumes-from=media -v=/path/to/annot:/var/www/annot -v=/path/to/crowbar.py:/var/www/annot/prjannot/crowbar.py -d -p 35627:80 elmar/apache_annot

8. With apache_annotdev docker container continuer with :ref:`step 6 to 9 form the User Manual Install Annot <link_developmer_manual>` instruction.

   
Run Annot for Development
^^^^^^^^^^^^^^^^^^^^^^^^^

* In one shell manipulate Annot source code on host server run apache_annotdev. Changes to the source code will immediately take effect. Hint: if the host is not your local computer, ssh into the host server to work with the source code. 

* In another shell enter apache_annot to control the apache web server. The following commands might be of use: ::

    docker exec -ti apache_annotdev /bin/bash
    
    /etc/init.d/postgresql
    /etc/init.d/postgresql status
    /etc/init.d/postgresql start
    /etc/init.d/postgresql restart
    /etc/init.d/postgresql stop

* Don't forget to add, commit, and push your changes to your git repository. And if your code runs, don't forget to merge request that the changes can be forked back into the official repository.


Software Philosophy and Style Guide
-----------------------------------

This is section is about coding style and philosophy.
If you like to contribute the project please stick to this software philosophy guide.
If you like to learn about philosophy and concept of the Annot project in the big picture pleas read the introduction chapter. 


Pep8?
^^^^^
.. Not really. Naturally over the years of coding one will definitely develop his one style. For the moment I like to value that. I think one can learn from each other’s style. Though this might change in the future. I will see how it will work out not explicitly to enforce pep8 for the project. 
.. You can learn from the otherones style.
.. Can I addapt the pep8 tool that it only checks what I want? 
.. Comment your code
.. The variable naming convention is most important to me. 

Brackets, Chars and Strings
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. Personally I like brackets. They make things clear. I like to introduce 

Variable and Constant Nameingconvention 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. hungarian notation 

Functions and Classes
^^^^^^^^^^^^^^^^^^^^^
.. docstring! spell checked.

Django App Nameingconvention
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. appon
.. appsa
.. appas_


Code Example: Implement a New Assay Workflow
---------------------------------------------
.. expectation somehow familiar with Python 3 and Django