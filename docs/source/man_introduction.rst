Introduction
============

Preface
-------
**In the past two decades**, technological developments have made it feasible to generate large volumes of heterogeneous biological data. Bioinformatics is an interdisciplinary field that develops methods and software tools for understanding and mining these large-scale and complex biological data. A major challenge in bioinformatics is the integration of data from different sources. Several efforts have been made to aid in this process, including: standard metadata annotation formats to share and describe the experiments (`ISA-Tab <http://www.isa-tools.org/>`_), minimal information guidelines to report biological and biomedical science (`MIBBI project <http://www.biosharing.org/>`_) and biological and biomedical ontology-based controlled vocabulary (`OBO Foundry <http://www.obofoundry.org/>`_ and `Bioontology <http://bioportal.bioontology.org/>`_). However, the bioinformatics community still struggles to bring these standards, guidelines, and ontologies into the wet laboratory, the workbench where the biological experiments are carried out.


Why Annot?
----------
**Wet lab scientists** use their lab books or traditional laboratory information management systems to keep track of the samples, reagents, assay workflow, provenance of results, and to record data. These interactions are usually focused on human beings, not machines. Consequently, as every **bioinformatician** knows, when sophisticated data analysis or data integration should be done, the data usually have to be "scrubbed" to be usable by computers. Unfortunately, this process is time-consuming and error-prone.

**A major part of the problem** is the lack adherence to a controlled vocabulary to annotate data of biological experiments. Even though annotating experiments with a controlled vocabulary would reduce common data integration errors, constant compliance is difficult to achieve using standard operating procedures.

**Here we introduce Annot**, a web application to bridge the gap between wet lab scientists and data analysts. Annot is a web application to captures the metadata, raw data, and processed data from biological studies in standardized computational formats so that they are ready for analysis and sharing.


Implementation
--------------

**The foundation of Annot** lies in the controlled vocabulary. Whenever possible, we derive controlled vocabulary from established ontologies that can be easily updated to the latest ontology version and extended to include missing terms. Internal terms then never interfere with official ontology terms. Each controlled vocabulary is handled as a django application.

Wet lab scientists had input on the structure of the **assay building bricks** (symbolized by the jigsaw pieces). Whenever possible, we adhered to the actual `LINCs metadata standard <http://www.lincsproject.org/data/data-standards/>`_ and `ISA-Tab specification <http://www.isa-tools.org/>`_.

**Assay workflows** can be connected by the assay building bricks analogous to a Unix pipeline, to track data provenance. This Lego-like building brick system makes it easy to keep the workflow up-to-date with actual assay development in the wet lab or add new assay workflows.

**Overall**, the system's goal is to be modular, adaptable, lean and agile but focused on the major task: capturing biological study metadata and data annotations so that data are ready for analysis and sharing.


.. image:: 20150320poster11architecture.png

**Figure:** Blueprint for the Annot interfaces and the modular Annot django project architecture. Investigation, study, assays, person, publication, protocol, reagent types, sample and each vocabulary are handled by a separate django application, represented by nested rectangles.


Source Code
-----------

The source code is distributed under the free and open source AGPLv3 license through https://gitlab.com/biotransistor/annot .

Annot is written in `Python 3.4 <https://www.python.org/>`_, utilizing the `Django 1.8 <https://www.djangoproject.com/>`_ web framework with `PostgreSQL 9.4 <http://www.postgresql.org/>`_ and `Apache 2.4 <https://projects.apache.org/projects/http_server.html>`_.
Annot is to deployed with the `Docker 1.6 <https://www.docker.com/>`_ distribution platform. All images are based on the officil `Debian 8 Jessie <https://www.debian.org/>`_ base image.
Early development and deployment evolved by running a `FreeBSD 10 <https://www.freebsd.org/>`_ operating system in a virtual machine. 


Project References
------------------
PyCon 2015 Montreal: https://us.pycon.org/2015/schedule/presentation/461/
