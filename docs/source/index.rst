.. Annot documentation master file, created by
   sphinx-quickstart on Sun May 10 19:42:06 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Annot's documentation!
=================================

| Annot is a web application to annotate bioscience experiments, to capture the experiments metadata and data so that data are ready for analysis and sharing.
| Annot is utterly modularly implemented to be adaptable to each laboratories specific needs.
| Annot is written in Python 3 with Django.
| The source code is distributed under the free and open source GNU AGPLv3 license, while this documentation is under the free and open source GNU FDLv1.3 license.

.. dont froget README.rst file with version log! readme.rst shoul link to readthedocs.org
       
Contents:

.. toctree::
    :maxdepth: 2

    man_introduction
    man_use
    man_development
    man_epilog
    agpl-3_0
    fdl-1_3

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
