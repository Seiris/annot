Epilog
======

Get Help
--------

Bugs and Issues
---------------

How-to Contribute
-----------------
You would like contribute the the project? Great! 
The following lines should get your ready.

Code
^^^^
.. Forking, Codeing, Pull Request
.. Unity Testing (opey the testing goat)
.. Pep8? yes and Check out this projecyts phylosphy and style guide.
.. don't forget you sellf to add you to the authorlist

Documentation
^^^^^^^^^^^^^
.. spellchecking
.. no trailing whitespaces   
.. don't forget you sellf to add you to the authorlist

 
Author List
-----------
In alphabetic order:

| Amanda Esch: wetlab knowledge antibody  
| Carly King: documentation work
| Damir Sudar: concept input
| David Kilburn: concept input
| Elmar Bucher (bue): main programmer
| Jim Korkola: wetlab knowledge MEMA assay
| Joe Gray: principal investigator
| Laura Heiser: principal investigator
| Mark Dane: concept input
| Michel Nederkof: concept input
| Nicholas Wang : concept input
| Rebekka Smith: wetlab knowledge protein and compounds
| Tiera Liby: wetlab knowledge biological samples
| Wally Thompson: wetlab knowledge protein and compounds

Who is Author?
^^^^^^^^^^^^^^
It does not matther if you contributes to the project by code or documentation, when your foke is ready to be merged with the official master branch add your self to the author list! There are as well a view author that have not contibutd by code or documentation, but by conceptual input an biological field knowledge which was important to the project.


Aknowledgement
--------------
This work was supported by the NIH LINCS center grant 1U54HG008100 to J.W.G.
