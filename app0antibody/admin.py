from django.contrib import admin
from app0antibody.models import AntibodyPrimaryBasicLevel, AntibodyPrimaryDetailLevel, AntibodySecondaryBasicLevel, AntibodySecondaryDetailLevel

# Register your models here.

# primary
#admin.site.register( AntibodyPrimaryBasicLevel )
class AntibodyPrimaryBasicLevelAdmin( admin.ModelAdmin ): 
    list_display = ("antibody_primary_name","antibody_primary_antigen","antibody_primary_host_organism")
    search_fields = ("antibody_primary_name","antibody_primary_antigen","antibody_primary_host_organism")
admin.site.register( AntibodyPrimaryBasicLevel, AntibodyPrimaryBasicLevelAdmin )

#admin.site.register( AntibodyPrimaryDetailLevel )
class AntibodyPrimaryDetailLevelAdmin( admin.ModelAdmin ): 
    list_display = ("antibody_primary_basiclevel","antibody_primary_detaillevel_serial","antibody_primary_registry_name","antibody_primary_registry_identifier","antibody_primary_provider","antibody_primary_provider_catalog_id","antibody_primary_provider_batch_id","antibody_primary_clonality","antibody_primary_clone_id","antibody_primary_isotype","antibody_primary_epitop_sequence","antibody_primary_epitop_sequence_note","antibody_primary_immunogen_sequence","antibody_primary_immunogen_sequence_note","antibody_primary_immunogen_source_organism","antibody_primary_yieldfraction","antibody_primary_purity_concentration_unit","antibody_primary_purity_concentration_value","antibody_primary_stocksolution_concentration_unit","antibody_primary_stocksolution_concentration_value","antibody_primary_dilution_factor_denominator","antobody_primary_reference_url","antibody_primary_notes")
    search_fields = ("antibody_primary_basiclevel","antibody_primary_detaillevel_serial","antibody_primary_registry_name","antibody_primary_registry_identifier","antibody_primary_provider","antibody_primary_provider_catalog_id","antibody_primary_provider_batch_id","antibody_primary_clonality","antibody_primary_clone_id","antibody_primary_isotype","antibody_primary_epitop_sequence","antibody_primary_epitop_sequence_note","antibody_primary_immunogen_sequence","antibody_primary_immunogen_sequence_note","antibody_primary_immunogen_source_organism","antibody_primary_yieldfraction","antibody_primary_purity_concentration_unit","antibody_primary_stocksolution_concentration_unit","antibody_primary_stocksolution_buffer","antibody_primary_dilution_buffer","antibody_primary_specifity","antibody_primary_notes")
    filter_horizontal = ("antibody_primary_stocksolution_buffer","antibody_primary_dilution_buffer")
    #filter_horizontal = ("antibody_primary_stocksolution_buffer","antibody_primary_dilution_buffer","antibody_primary_specifity")
admin.site.register( AntibodyPrimaryDetailLevel, AntibodyPrimaryDetailLevelAdmin )


# secundary 
#admin.site.register( AntibodySecondaryBasicLevel )
class AntibodySecondaryBasicLevelAdmin( admin.ModelAdmin ): 
    list_display = ("antibody_secondary_name","antibody_secondary_target_organism","antibody_secondary_dye","antibody_secondary_color","antibody_secondary_host_organism")  # antibody_secondary_wavelength
    search_fields = ("antibody_secondary_name","antibody_secondary_target_organism","antibody_secondary_dye","antibody_secondary_color","antibody_secondary_host_organism") 
admin.site.register( AntibodySecondaryBasicLevel, AntibodySecondaryBasicLevelAdmin )

#admin.site.register( AntibodySecondaryDetailLevel )
class AntibodySecondaryDetailLevelAdmin( admin.ModelAdmin ): 
    list_display = ("antibody_secondary_basiclevel","antibody_secondary_detaillevel_serial","antibody_secondary_registry_name","antibody_secondary_registry_identifier","antibody_secondary_provider","antibody_secondary_provider_catalog_id","antibody_secondary_provider_batch_id","antibody_secondary_stocksolution_concentration_unit","antibody_secondary_stocksolution_concentration_value","antibody_secondary_dilution_factor_denominator","antobody_secondary_reference_url","antibody_secondary_notes")
    search_fields = ("antibody_secondary_basiclevel","antibody_secondary_detaillevel_serial","antibody_secondary_registry_name","antibody_secondary_registry_identifier","antibody_secondary_provider","antibody_secondary_provider_catalog_id","antibody_secondary_provider_batch_id","antibody_secondary_stocksolution_concentration_unit","antibody_secondary_stocksolution_buffer","antibody_secondary_dilution_buffer","antibody_secondary_specifity","antibody_secondary_notes") 
    filter_horizontal = ("antibody_secondary_stocksolution_buffer","antibody_secondary_dilution_buffer","antibody_secondary_specifity")
admin.site.register( AntibodySecondaryDetailLevel, AntibodySecondaryDetailLevelAdmin )
