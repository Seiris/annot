from django.db import models
# import ontologies
from apponantibodyname_own.models import AntibodyName
from apponclonality_bioontology.models import Clonality 
from apponcolor_own.models import Color
from apponimmunologyisotype_bioontology.models import ImmunologyIsotype  # bao antibody isotype
from apponorganism_bioontology.models import Organism
from apponprotein_uniprot.models import Protein
from apponprovider_own.models import Provider
from apponcompound_bioontology.models import Compound  # dye and dilutant
from apponunit_bioontology.models import Unit
from apponyieldfraction_own.models import YieldFraction
# import bridges 
from app0sample.models import SampleBasicLevel


# Create your models here.
# primary antibody 
class AntibodyPrimaryBasicLevel( models.Model ):
    antibody_primary_name = models.ForeignKey( AntibodyName, primary_key=True, default="not_yet_specified", verbose_name="Primary antibody name.", help_text="The name for this primary antibody.") # If possible stick to the WHO/USAN nameing convention: https://en.wikipedia.org/wiki/Nomenclature_of_monoclonal_antibodies 
    antibody_primary_antigen = models.ForeignKey( Protein, default="not_yet_specified", verbose_name="Antigen", help_text="Choose target protein.")  # verbose_name
    antibody_primary_host_organism = models.ForeignKey( Organism, default="not_yet_specified", verbose_name="Host species", help_text="Chosse host specices.")
    def __str__( self ):
        s_out = self.antibody_primary_name.annot_id
        return( s_out ) 
    class Meta: 
        unique_together = (("antibody_primary_antigen","antibody_primary_host_organism"),)
        ordering = ["antibody_primary_antigen_id","antibody_primary_host_organism_id"]
        verbose_name_plural="Primary antibodies basic level"


class AntibodyPrimaryDetailLevelManager( models.Manager):
    def get_by_natural_key( self, antibody_primary_basiclevel, antibody_primary_detaillevel_serial ):
        return self.get( antibody_primary_basiclevel=antibody_primary_basiclevel, antibody_primary_detaillevel_serial=antibody_primary_detaillevel_serial )

class AntibodyPrimaryDetailLevel( models.Model ): 
    objects = AntibodyPrimaryDetailLevelManager()
    antibody_primary_basiclevel = models.ForeignKey( AntibodyPrimaryBasicLevel, default="not_yet_specified", verbose_name="Primary antibody", help_text="Choose primary antibody.")
    antibody_primary_detaillevel_serial = models.SlugField( default="not_yet_specified", verbose_name="Primary antibody detail level serial number", help_text="Unique serial number for this primary antibody. Only alphanumeric keys and underscore allowed." )
    antibody_primary_registry_name = models.CharField( max_length=255, verbose_name="Antibody registry antibody name", help_text="Official http://antibodyregistry.org/ antibody name for lincs project related antibody.", blank=True )
    antibody_primary_registry_identifier = models.CharField( max_length=255, verbose_name="Antibody registry antibody identifier", help_text="Official http://antibodyregistry.org/ antibody identifier for lincs project related antibody.", blank=True )
    antibody_primary_provider = models.ForeignKey( Provider, related_name="AntibodyPrimaryProvider", default="not_yet_specified", verbose_name="Provider", help_text="Choose antibody provider." )
    antibody_primary_provider_catalog_id = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Provider catalog id", help_text="ID or catalogue number or name assigned to the antibody by the provider." ) 
    antibody_primary_provider_batch_id = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Provider batch id", help_text="Batch or lot number assigned to the antibody by the provider." )
    antibody_primary_clonality = models.ForeignKey( Clonality, related_name="AntibodyPrimaryClonality", default="not_yet_specified", verbose_name="Clonalety", help_text="Chosse clonalety.")
    antibody_primary_clone_id = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Provider clone id", help_text="If monoclonal specify clone." )
    antibody_primary_isotype = models.ForeignKey( ImmunologyIsotype, related_name="AntibodyPrimaryIsotype", default="not_yet_specified", verbose_name="Immunology isotype", help_text="Choose isotype.")
    antibody_primary_epitop_sequence = models.TextField( default="not_yet_specified", verbose_name="Epitop sequence", help_text="Fasta format sequence of epitop.", blank=True ) 
    antibody_primary_epitop_sequence_note = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Epitop sequence note", help_text="Epitop sequence provider note.", blank=True )
    antibody_primary_immunogen_sequence = models.TextField( default="not_yet_specified", verbose_name="Immunogen sequence", help_text="Fasta format sequence of immunogen.", blank=True ) 
    antibody_primary_immunogen_sequence_note = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Immunogen sequence note", help_text="Immunogen sequence provider note.", blank=True )
    antibody_primary_immunogen_source_organism = models.ForeignKey( SampleBasicLevel, related_name="AntibodyPrimaryImmunogenSourceOrganism", default="not_yet_specified", verbose_name="Immunogen source", help_text="Specify immunogen production source organism or cell line." )
    antibody_primary_yieldfraction = models.ForeignKey( YieldFraction, related_name="AntibodyPrimaryYieldFraction", default="not_yet_specified", verbose_name="Antibody yield fraction", help_text="Host organism antibody yield fraction." )  # purified, supernatant, ascites
    antibody_primary_purity_concentration_unit = models.ForeignKey( Unit, related_name="AntibodyPrimaryPurityConcentrationUnit", default="not_yet_specified", verbose_name="Purity concentration unit", help_text="If purified antibody, purity value unit. " ) 
    antibody_primary_purity_concentration_value = models.FloatField( default=None, verbose_name="Purity concentration value", help_text="If purified antibody, purity value.", null=True, blank=True )
    antibody_primary_stocksolution_concentration_unit = models.ForeignKey( Unit, related_name="AntibodyPrimaryStocksolutionConcentrationUnit", default="not_yet_specified", verbose_name="Stock solution concentration unit", help_text="Value unit." ) 
    antibody_primary_stocksolution_concentration_value = models.FloatField( default=None, verbose_name="Stock solution concentration value", help_text="Value.", null=True, blank=True )
    antibody_primary_stocksolution_buffer = models.ManyToManyField( Compound, related_name="AntibodyPrimaryStocksolutionBuffer", verbose_name="Stock solution buffer compounds", help_text="Choose all utilised stock buffer compounds" )
    antibody_primary_dilution_factor_denominator = models.PositiveIntegerField( default=None, verbose_name="Dilution factor 1/n", help_text="Dilsuion factor denominator n.", null=True, blank=True ) 
    antibody_primary_dilution_buffer = models.ManyToManyField( Compound, related_name="AntibodyPrimaryDilutionBuffer", verbose_name="Dilution buffer compounds", help_text="Choose all utilised dilutant compounds" )
    antibody_primary_specifity = models.ManyToManyField( Protein, related_name="AntibodyPrimarySpecifity", verbose_name="Crossreactivity", help_text="Proteins which this primary antibody is able to detect." )
    antobody_primary_reference_url = models.URLField( default="https://not.yet.specified", verbose_name="Reference URL", help_text="URL to the vendor or provider secific antibody specification." )
    antibody_primary_notes = models.TextField( verbose_name="Notes", help_text="Additional this primary antibody related notes", blank=True ) # ph, temperature
    def __str__( self ):
        s_out = self.antibody_primary_basiclevel.antibody_primary_name.annot_id+'-'+self.antibody_primary_detaillevel_serial
        return( s_out )
    def natural_key( self ):
        return( self.antibody_primary_basiclevel.pk, self.antibody_primary_detaillevel_serial )
    natural_key.dependencies = ["app0antibody.AntibodyPrimaryBasicLevel"]

    class Meta: 
        unique_together = (("antibody_primary_basiclevel", "antibody_primary_detaillevel_serial"),)
        ordering = ["antibody_primary_basiclevel_id","antibody_primary_detaillevel_serial"]
        verbose_name_plural="Primary antibodies detail level"



# seconday antibody 
class AntibodySecondaryBasicLevel( models.Model ):
    antibody_secondary_name = models.ForeignKey( AntibodyName, primary_key=True, default="not_yet_specified", verbose_name="Secondary antibody name.", help_text="The name for this secondary antbody." ) # If possible stick to the WHO/USAN nameing convention: https://en.wikipedia.org/wiki/Nomenclature_of_monoclonal_antibodies 
    antibody_secondary_target_organism = models.ForeignKey( Organism, default="not_yet_specified", related_name="TragetOrganism", verbose_name="Target species", help_text="Chosse antibody target specices." )
    antibody_secondary_dye = models.ForeignKey( Compound, default="not_yet_specified", verbose_name="Dye compound", help_text="Choose fluorophore dye. If the compount does not exist in the controlled vocabulary, choose a term compatible with http://pubchem.ncbi.nlm.nih.gov/ compound or substance.  If the compund not exist at PubChem as well, choose a therm compatible with http://www.spectra.arizona.edu/ ." ) 
    antibody_secondary_dayheader = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Dye output header", help_text="Explicite dye nameing as it occures in the column header in the output file." )
    antibody_secondary_color = models.ForeignKey( Color, default="not_yet_specified", verbose_name="Dye visualization color", help_text="Choose visualization color." )  # Stick as closed as possible to the fluorophore wavelength
    antibody_secondary_host_organism = models.ForeignKey( Organism, default="not_yet_specified", related_name="HostOrganism", verbose_name="Host species", help_text="Choose host specices." )
    def __str__( self ):
        s_out = self.antibody_secondary_name.annot_id
        return( s_out ) 
    class Meta: 
        unique_together = (("antibody_secondary_name","antibody_secondary_target_organism","antibody_secondary_dye","antibody_secondary_host_organism"),)  # antibody_secondary_wavelength, antibody_secondary_color
        ordering = ["antibody_secondary_name_id","antibody_secondary_target_organism_id","antibody_secondary_dye_id","antibody_secondary_host_organism_id"]
        verbose_name_plural="Secondary antibodies basic level" 


class AntibodySecondaryDetailLevelManager( models.Manager):
    def get_by_natural_key( self, antibody_secondary_basiclevel, antibody_secondary_detaillevel_serial ):
        return self.get( antibody_secondary_basiclevel=antibody_secondary_basiclevel, antibody_secondary_detaillevel_serial=antibody_secondary_detaillevel_serial )

class AntibodySecondaryDetailLevel( models.Model ): 
    objects = AntibodySecondaryDetailLevelManager()
    antibody_secondary_basiclevel  = models.ForeignKey( AntibodySecondaryBasicLevel, default="not_yet_specified", verbose_name="Secondary antibody", help_text="Choose secondary antibody.")
    antibody_secondary_detaillevel_serial = models.SlugField( default="not_yet_specified", verbose_name="Secondary antibody detail level serial number", help_text="Uniqe serial number for this secondary antibody. Only alphanumeric keys and underscore allowed." )
    antibody_secondary_registry_name = models.CharField( max_length=255, verbose_name="Antibody registry antibody name", help_text="Official http://antibodyregistry.org/ antibody name for lincs project related antibody.", blank=True )
    antibody_secondary_registry_identifier = models.CharField( max_length=255, verbose_name="Antibody registry antibody identifier", help_text="Official http://antibodyregistry.org/ antibody identifier for lincs project related antibody.", blank=True )
    antibody_secondary_provider = models.ForeignKey( Provider, related_name="AntibodySecondaryProvider", default="not_yet_specified", verbose_name="Provider", help_text="Choose antibody provider." )
    antibody_secondary_provider_catalog_id = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Provider catalog id", help_text="ID or catalogue number or name assigned to the antibody by the provider." ) 
    antibody_secondary_provider_batch_id = models.CharField( max_length=255, default="not_yet_specified", verbose_name="Provider batch id", help_text="Batch or lot number assigned to the antibody by the provider." )
    #antibody_secondary_clonality = models.ForeignKey( Clonality, related_name="AntibodySecondaryClonality", verbose_name="Clonalety", help_text="Choose clonalety." )
    #antibody_secondary_provider_clone_id = models.CharField(  verbose_name="Provider clone id", max_length=255, help_text="If monoclonal specify clone." )
    #antibody_secondary_isotype = models.ForeignKey( ImmunologyIsotype, related_name="AntibodySecondaryIsotype", verbose_name="Immunology isotype", help_text="Choose isotype." )
    #antibody_secondary_yieldfraction = models.ForeignKey( YieldFraction, related_name="AntibodySecondaryYieldFraction", verbose_name="Antibody yield fraction", help_text="Host organism antibody yield fraction." )  # purified, supernatant, ascites
    #antibody_secondary_purity_concentration_unit = models.ForeignKey( Unit, related_name="AntibodySecondaryPurityConcentrationUnit", verbose_name="Purity concentration unit", help_text="If purified antibody, purity value unit. " ) 
    #antibody_secondary_purity_concentration_value = models.FloatField( verbose_name="Purity concentration value", help_text="If purified antibody, purity value." )
    antibody_secondary_stocksolution_concentration_unit = models.ForeignKey( Unit, default="not_yet_specified", related_name="AntibodySecondaryStocksolutionConcentrationUnit", verbose_name="Stock solution concentration unit", help_text="Value unit." ) 
    antibody_secondary_stocksolution_concentration_value = models.FloatField( default=None, verbose_name="Stock solution concentration value", help_text="Value.", null=True, blank=True )
    antibody_secondary_stocksolution_buffer = models.ManyToManyField( Compound, related_name="AntibodySecondaryStocksolutionBuffer", verbose_name="Stock solution buffer compounds", help_text="Choose all utilised stock buffer compounds" )
    antibody_secondary_dilution_factor_denominator = models.PositiveIntegerField( default=None, verbose_name="Dilution factor 1/n", help_text="Dilsuion factor denominator n.", null=True, blank=True ) 
    antibody_secondary_dilution_buffer = models.ManyToManyField( Compound, related_name="AntibodySecondaryDilutionBuffer", verbose_name="Dilution buffer compounds", help_text="Choose all utilised dilutant compounds" )
    antibody_secondary_specifity = models.ManyToManyField( Organism, related_name="AntibodySecondarySpecifity" , verbose_name="Crossadsorbed", help_text="Organism which this secondary antibody is able to detect." )
    antobody_secondary_reference_url = models.URLField( default="https://not.yet.specified", verbose_name="Reference URL", help_text="URL to the vendor or provider secific antibody specification." )
    antibody_secondary_notes = models.TextField( verbose_name="Notes", help_text="Additional this secondary  antibody related notes", blank=True ) # ph, temperature,
    def __str__( self ):
        s_out = self.antibody_secondary_basiclevel.antibody_secondary_name.annot_id+'-'+self.antibody_secondary_detaillevel_serial
        return( s_out ) 
    def natural_key( self ):
        return( self.antibody_secondary_basiclevel.pk, self.antibody_secondary_detaillevel_serial )
    natural_key.dependencies = ["app0antibody.AntibodySecondaryBasicLevel"]

    class Meta: 
        unique_together = (("antibody_secondary_basiclevel", "antibody_secondary_detaillevel_serial"),)
        ordering = ["antibody_secondary_basiclevel_id","antibody_secondary_detaillevel_serial"]
        verbose_name_plural="Secondary antibodies detail level"
