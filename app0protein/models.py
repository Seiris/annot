from django.db import models
# import ontologies
from apponprotein_uniprot.models import Protein
from apponproteinsetname_own.models import ProteinSetName
from apponprovider_own.models import Provider
from apponunit_bioontology.models import Unit
from apponcompound_bioontology.models import Compound
# import bridges
from app0sample.models import SampleBasicLevel



# protein 
class ProteinBasicLevel( models.Model ):
    protein = models.ForeignKey( Protein, primary_key=True, default="not_yet_specified", verbose_name="Protein", help_text="Choose reference protein." )
    protein_lincs_name = models.CharField( max_length=255, verbose_name="Lincs sample name", help_text="Official lincs protein name for lincs prj related protein.", blank=True )
    protein_lincs_identifier = models.CharField( max_length=255, verbose_name="Lincs sample identifier", help_text="Official lincs protein identifier for lincs prj related protein.", blank=True )
    protein_reference_length = models.PositiveIntegerField( default=None, verbose_name="Reference protein length", help_text="Referenc protein length in number amino acids.", null=True, blank=True )
    protein_reference_mass = models.FloatField( default=None, verbose_name="Reference protein mass", help_text="Referenc protein mass in kDa.", null=True, blank=True )
    def __str__( self ):
        return( self.protein.annot_id )

    class Meta:
        ordering = ["protein"]

        
class ProteinDetailLevelManager( models.Manager ): 
    def get_by_natural_key( self, protein_basiclevel, protein_detaillevel_serial ):
        return self.get( protein_basiclevel=protein_basiclevel, protein_detaillevel_serial=protein_detaillevel_serial )

class ProteinDetailLevel( models.Model ): 
    objects = ProteinDetailLevelManager()
    protein_basiclevel = models.ForeignKey( ProteinBasicLevel, default="not_yet_specified", verbose_name="Protein", help_text="Choose reference protein." )
    protein_detaillevel_serial = models.SlugField( default="not_yet_specified", verbose_name="Protein detail level serial number", help_text="Unique serial number for this particular protein reagent. Only alphanumeric keys and underscore allowed." ) 
    protein_provider = models.ForeignKey( Provider, default="not_yet_specified", verbose_name="Provider", help_text="Choose protein provider." )
    protein_provider_catalog_id = models.CharField( default="not_yet_specified", verbose_name="Provider catalog protein id", max_length=255, help_text="ID or catalogue number or name assigned to the protein by the vendor or provider." ) # SDTM : Sample Name [IDVAR=LBREFID]     
    protein_provider_batch_id = models.CharField( default="not_yet_specified", verbose_name="Provider batch id", max_length=255, help_text="Batch or lot number assigned to the protein by the vendor or provider." )  
    protein_real_sequence_note = models.CharField( default="not_yet_specified", verbose_name="Real protein sequence note", max_length=255, help_text="Protein sequence provider note." ) 
    protein_real_sequence = models.TextField( default="not_yet_specified", verbose_name="Real protein sequence",help_text="Exact fasta format sequence of protein used in the lab." ) 
    protein_real_length = models.PositiveIntegerField( default=None, verbose_name="Real protein length", help_text="Real protein length in number amino acids.", null=True, blank=True )
    protein_real_mass = models.FloatField( default=None, verbose_name="Real protein mass", help_text="Real protein mass in kDa. If a mass range is given, take the mean value.", null=True, blank=True )
    protein_source_organism = models.ForeignKey( SampleBasicLevel, default="not_yet_specified", verbose_name="Real protein production source", help_text="Specify protein production source organism or cell line." ) 
    protein_purity = models.FloatField( default=None, verbose_name="Real protein purity", help_text="Purity in percent. If caused by different purity detection methodes more then one purity value is given, choose the worst purity value.", null=True, blank=True ) 
    protein_stocksolution_concentration_unit = models.ForeignKey( Unit,  default="not_yet_specified", verbose_name="Stock solution concentration unit", help_text="Value unit." ) 
    protein_stocksolution_concentration_value = models.FloatField( default=None, verbose_name="Stock solution concentration value", help_text="Value.", null=True, blank=True )
    protein_stocksolution_buffer = models.ManyToManyField( Compound, related_name="ProteinStocksolutionBuffer", verbose_name="Stock solution buffer compounds", help_text="Choose all utilised stock buffer compounds" )
    protein_dilution_factor_denominator = models.PositiveIntegerField( default=None, verbose_name="Dilution factor 1/n", help_text="Dilsuion factor denominator n.", null=True, blank=True )
    protein_dilution_buffer = models.ManyToManyField( Compound, related_name="ProteinDilutionBuffer", verbose_name="Dilution buffer compounds", help_text="Choose all utilised dilutant compounds" )
    protein_reference_url = models.URLField( default="https://not.yet.specified", verbose_name="Proten reference URL", help_text="URL to the vendor or provider secific protein specification." ) 
    protein_notes = models.TextField( verbose_name="Notes", help_text="Additional this protein related notes", blank=True ) # ph, stock concentration value, stock storage temperature,
    def __str__( self ):
        s_out = self.protein_basiclevel.protein.annot_id+'-'+self.protein_detaillevel_serial
        return( s_out )
    def natural_key( self ):
        return( self.protein_basiclevel.pk, self.protein_detaillevel_serial )
    natural_key.dependencies = ["app0protein.ProteinBasicLevel"]

    class Meta:
        unique_together = (("protein_basiclevel", "protein_detaillevel_serial"),)
        ordering = ["protein_basiclevel","protein_detaillevel_serial"]

        

# protein complex and protein set handling
class ProteinSetProteinDetailLevelManager( models.Manager ): 
    def get_by_natural_key( self, protein_basiclevel, proteinsetprotein_serial ):
        return self.get( protein_basiclevel=protein_basiclevel, proteinsetprotein_serial=proteinsetprotein_serial )

class ProteinSetProteinDetailLevel( models.Model ):
    objects = ProteinSetProteinDetailLevelManager()
    protein_basiclevel = models.ForeignKey( ProteinBasicLevel, default="not_yet_specified", verbose_name="Protein", help_text="Choose reference protein." )
    proteinsetprotein_serial = models.SlugField( default="not_yet_specified", verbose_name="Protein set protein serial number", help_text="Unique serial number for this particular protein set protein reagent. Only alphanumeric keys and underscore allowed." )
    proteinsetprotein_provider = models.ForeignKey( Provider, default="not_yet_specified", verbose_name="Provider", help_text="Choose protein set provider." )
    proteinsetprotein_provider_catalog_id = models.CharField( default="not_yet_specified", verbose_name="Provider catalog protein set id", max_length=255, help_text="ID or catalogue number or name assigned to the protein set by the vendor or provider." ) # SDTM : Sample Name [IDVAR=LBREFID]     
    proteinsetprotein_real_sequence_note = models.CharField( default="not_yet_specified", verbose_name="Real protein set protein sequence note", max_length=255, help_text="Protein sequence provider note." ) 
    proteinsetprotein_real_sequence = models.TextField( default="not_yet_specified", verbose_name="Real protein set protein sequence",help_text="Exact fasta format sequence of protein used in the lab." ) 
    proteinsetprotein_real_length = models.PositiveIntegerField( default=None, verbose_name="Real protein set protein length", help_text="Real protein length in number amino acids.", null=True, blank=True )
    proteinsetprotein_real_mass = models.FloatField( default=None, verbose_name="Real protein set protein mass", help_text="Real protein mass in kDa. If a mass range is given, take the mean value.", null=True, blank=True )
    proteinsetprotein_source_organism = models.ForeignKey( SampleBasicLevel, default="not_yet_specified", verbose_name="Real protein production source", help_text="Specify protein production source organism or cell line." ) 
    proteinsetprotein_purity = models.FloatField( default=None, verbose_name="Real protein set protein purity", help_text="Purity in percent. If caused by different purity detection methodes more then one purity value is given, choose the worst purity value.", null=True, blank=True ) 
    def __str__( self ):
        s_out = self.protein_basiclevel.protein.annot_id+'-'+self.proteinsetprotein_serial
        return( s_out )
    def natural_key( self ):
        return( self.protein_basiclevel.pk, self.proteinsetprotein_serial )
    natural_key.dependencies = ["app0protein.ProteinBasicLevel"]

    class Meta:
        unique_together = (("protein_basiclevel", "proteinsetprotein_serial"),)
        ordering = ["protein_basiclevel","proteinsetprotein_serial"]


class ProteinSetDetailLevelManager( models.Manager ): 
    def get_by_natural_key( self, proteinsetname, proteinset_detaillevel_serial ):
        return self.get( proteinsetname=proteinsetname, proteinset_detaillevel_serial=proteinset_detaillevel_serial )

class ProteinSetDetailLevel( models.Model ): 
    objects = ProteinSetDetailLevelManager()
    proteinsetname = models.ForeignKey( ProteinSetName, default="not_yet_specified", verbose_name="Protein set name", help_text="Choose protein set label." )
    proteinset_detaillevel_serial = models.SlugField( default="not_yet_specified", verbose_name="Protein detail level serial number", help_text="Unique serial number for this particular protein set. Only alphanumeric keys and underscore allowed." ) 
    proteinsetprotein_detaillevel = models.ManyToManyField( ProteinSetProteinDetailLevel, verbose_name="Protein set protein", help_text="Choose protein set protein." )
    proteinset_provider_batch_id = models.CharField( default="not_yet_specified", verbose_name="Provider batch id", max_length=255, help_text="Batch or lot number assigned to the protein set by the vendor or provider." )  
    proteinset_stocksolution_concentration_unit = models.ForeignKey( Unit,  default="not_yet_specified", verbose_name="Stock solution concentration unit", help_text="Value unit." ) 
    proteinset_stocksolution_concentration_value = models.FloatField( default=None, verbose_name="Stock solution concentration value", help_text="Value.", null=True, blank=True )
    proteinset_stocksolution_buffer = models.ManyToManyField( Compound, related_name="ProteinSetStocksolutionBuffer", verbose_name="Stock solution buffer compounds", help_text="Choose all utilised stock buffer compounds" )
    proteinset_dilution_factor_denominator = models.PositiveIntegerField( default=None, verbose_name="Dilution factor 1/n", help_text="Dilsuion factor denominator n.", null=True, blank=True )
    proteinset_dilution_buffer = models.ManyToManyField( Compound, related_name="ProteinSetDilutionBuffer", verbose_name="Dilution buffer compounds", help_text="Choose all utilised dilutant compounds" )
    proteinset_reference_url = models.URLField( default="https://not.yet.specified", verbose_name="Proten set reference URL", help_text="URL to the vendor or provider secific protein set specification." ) 
    proteinset_notes = models.TextField( verbose_name="Notes", help_text="Additional this protein set related notes", blank=True ) # ph, stock concentration value, stock storage temperature,
    def __str__( self ):
        s_out = self.proteinsetname.annot_id+'-'+self.proteinset_detaillevel_serial
        return( s_out )
    def natural_key( self ):
        return( self.proteinsetname.pk, self.proteinset_detaillevel_serial )

    class Meta:
        unique_together = (("proteinsetname", "proteinset_detaillevel_serial"),)
        ordering = ["proteinsetname","proteinset_detaillevel_serial"]        
    natural_key.dependencies = ["apponproteinsetname_own.ProteinSetName"] #"app0protein.ProteinBasicLevel","app0protein.ProteinSetProteinDetailLevel" 
