from django.contrib import admin
from app0protein.models import ProteinBasicLevel, ProteinDetailLevel, ProteinSetProteinDetailLevel, ProteinSetDetailLevel

# Register your models here.
#admin.site.register( ProteinBasicLevel )
class ProteinBasicLevelAdmin( admin.ModelAdmin ): 
    list_display = ("protein","protein_lincs_name","protein_lincs_identifier","protein_reference_length","protein_reference_mass")
    search_fields = ("protein","protein_lincs_name","protein_lincs_identifier")
admin.site.register( ProteinBasicLevel, ProteinBasicLevelAdmin )

#admin.site.register( ProteinDetailLevel )
class ProteinDetailLevelAdmin( admin.ModelAdmin ): 
    list_display = ("protein_basiclevel","protein_detaillevel_serial","protein_provider","protein_provider_catalog_id","protein_provider_batch_id","protein_real_sequence_note","protein_real_sequence","protein_real_length","protein_real_mass","protein_source_organism","protein_purity","protein_stocksolution_concentration_unit","protein_stocksolution_concentration_value","protein_dilution_factor_denominator","protein_reference_url","protein_notes")
    search_fields = ("protein_basiclevel","protein_detaillevel_serial","protein_provider","protein_provider_catalog_id","protein_provider_batch_id","protein_real_sequence_note","protein_real_sequence","protein_source_organism","protein_stocksolution_concentration_unit","protein_stocksolution_buffer","protein_dilution_buffer","protein_notes")
    filter_horizontal = ("protein_stocksolution_buffer","protein_dilution_buffer")
admin.site.register( ProteinDetailLevel, ProteinDetailLevelAdmin )

#admin.site.register( ProteinSetProteinDetailLevel )
class ProteinSetProteinDetailLevelAdmin( admin.ModelAdmin ): 
    list_display = ("protein_basiclevel","proteinsetprotein_serial","proteinsetprotein_provider","proteinsetprotein_provider_catalog_id","proteinsetprotein_real_sequence_note","proteinsetprotein_real_sequence","proteinsetprotein_real_length","proteinsetprotein_real_mass","proteinsetprotein_source_organism","proteinsetprotein_purity")
    search_fields = ("protein_basiclevel","proteinsetprotein_serial","proteinsetprotein_provider","proteinsetprotein_provider_catalog_id","proteinsetprotein_real_sequence_note","proteinsetprotein_real_sequence","proteinsetprotein_source_organism")
admin.site.register( ProteinSetProteinDetailLevel, ProteinSetProteinDetailLevelAdmin )

#admin.site.register( ProteinSetDetailLevel )
class ProteinSetDetailLevelAdmin( admin.ModelAdmin ): 
    list_display = ("proteinsetname","proteinset_detaillevel_serial","proteinset_provider_batch_id","proteinset_stocksolution_concentration_unit","proteinset_stocksolution_concentration_value","proteinset_dilution_factor_denominator","proteinset_reference_url","proteinset_notes")
    search_fields = ("proteinsetname","proteinsetprotein_basiclevel","proteinset_detaillevel_serial","proteinset_provider_batch_id","proteinset_stocksolution_concentration_unit","proteinset_stocksolution_buffer","proteinset_dilution_buffer","proteinset_notes") 
    filter_horizontal = ("proteinsetprotein_detaillevel","proteinset_stocksolution_buffer","proteinset_dilution_buffer")
admin.site.register( ProteinSetDetailLevel, ProteinSetDetailLevelAdmin )
