from django.db import models
# import ontologies
# basic level
from apponsamplename_own.models import SampleName
from apponsampletype_own.models import SampleType
from apponorganism_bioontology.models import Organism
from apponsex_bioontology.models import Sex
from apponethnicity_bioontology.models import Ethnicity
from apponhealthstatus_own.models import HealthStatus
from appondisease_bioontology.models import Disease
from apponorgan_bioontology.models import Organ
from appontissue_bioontology.models import Tissue
from apponcelltype_bioontology.models import CellType 
from appongrowthproperty_bioontology.models import GrowthProperty
from apponcellmarker_own.models import CellMarker
from apponmutation_own.models import Mutation
from appongeneticmodification_bioontology.models import GeneticModification
# detail level
from apponprovider_own.models import Provider
# lab level 
from apponverificationprofile_own.models import VerificationProfile
from appontransientmodification_bioontology.models import TransientModification
# single sample level
from apponunit_bioontology.models import Unit
# sample set level
from apponsamplesetname_own.models import SampleSetName
# import bridges
from app0publication.models import Publication
from app0person.models import Person



# Create your models here.

# sample 
# bue 20141119: does not suport source (insted sample_parent field)
# bue 20141119: does not suport SDTM standard 
class SampleBasicLevel( models.Model ):  #
    # sample
    # sample name
    #sample_sdtm = BooleanField( default=False, help_text="SDTM Study Data Tabulation Model annotated sample?" )
    sample_name = models.ForeignKey( SampleName, primary_key=True, default="not_yet_specified", help_text="The name for the primary cells or cell line." ) # SDTM : Source Name [USUBJID]
    sample_lincs_name = models.CharField( max_length=255, verbose_name="Lincs sample name", help_text="Official lincs sample name for lincs project related samples.", blank=True )
    sample_lincs_identifier = models.CharField( max_length=255, verbose_name="Lincs sample identifier", help_text="Official lincs sample identifier  for lincs prj related samples.", blank=True)
    # biology features
    sample_material_type = models.ForeignKey( SampleType, default="not_yet_specified", help_text="e.g. whole organism, organism part" )
    sample_organism = models.ForeignKey( Organism, default="not_yet_specified", verbose_name="Organism", help_text="Organism of origin from which the cell was derived." )  # characteristic 
    sample_organism_sex = models.ForeignKey( Sex, default="not_yet_specified", verbose_name="Sex", help_text="The sex of the organism from which the cell was obtained." )  # characteristic  
    sample_organism_ethnicity = models.ManyToManyField( Ethnicity, verbose_name="Ethnicity", help_text="The ethnicity of the donor." )  # characteristic; bue 20150225: sorry with manytomany fields i dont get a proper default running  
    sample_organism_age_year = models.FloatField( default=None, verbose_name="Age [year]", help_text="The age of the donor.", null=True, blank=True )  # characteristic  
    sample_health_status = models.ForeignKey( HealthStatus, default="not_yet_specified", verbose_name="Health status", help_text="The health status: healthy sample from a healthy person, normal sample from a ill person, disease sample.")  # characteristic 
    sample_disease = models.ManyToManyField( Disease, verbose_name="Disease", help_text="If the primary cell came from a particular diseased tissue, the disease should be noted." )  # characteristic and sample_characteristic_disease_detail 
    sample_organ = models.ForeignKey(  Organ, default="not_yet_specified", verbose_name="Organ", help_text="Organ of origin from which the cell was derived." )  # characteristic 
    sample_tissue = models.ForeignKey( Tissue, default="not_yet_specified", verbose_name="Tissue", help_text="Tissue of origin from which the cell was derived." )  # characteristic 
    sample_cell_type = models.ForeignKey( CellType, default="not_yet_specified", verbose_name="Cell type", help_text="The cell type from which a cell was derived, also sometimes referred to as cell morphology." )  # characteristic and sample_characteristic_cell_type_detail 
    sample_growth_propertie = models.ManyToManyField( GrowthProperty, verbose_name="Growth propertie", help_text="The growth properties of the cell." )  # characteristic 
    # biological identification
    sample_cell_marker = models.ManyToManyField( CellMarker, verbose_name="Cell marker", help_text="The markers used to isolate and identify the cell type." )  # characteristic 
    sample_verification_profile_reference = models.URLField( default="https://not.yet.specified", verbose_name="Verification profile refernce URL", help_text="Link to expected STR (reference) profile of the cell based on provider information." )  # characteristic 
    sample_mutation_reference = models.URLField( default="https://not.yet.specified", verbose_name="Mutation reference URL", help_text="Link to known mutation in cell from a reference." )  # characteristic 
    sample_mutation_explicite = models.ManyToManyField( Mutation, verbose_name="Mutation explicite", help_text="Known mutation in primary cell captured explicitly." )  # characteristic 
    # biological egineering
    sample_genetic_modification = models.ManyToManyField( GeneticModification, verbose_name="Genetic modification", help_text="If the cell was  stable transfection or viral transduction, the modifications (e.g. expressing GFP-tagged protein) should be described and appropriate references provided." )  # characteristic 
    # literature 
    sample_original_publication = models.ForeignKey( Publication, default="not_yet_specified", verbose_name="Original publication", help_text="Publication where the cell line for the fist time was mentioned." )  # characteristic
    sample_culture_conditions_recommended = models.URLField( default="https://not.yet.specified", verbose_name="Culture conditions recommended URL", help_text="Link to description of the standard tissue culture conditions (media, supplements, culture dish treatment) used to maintain the cell." )  # characteristic 
    sample_notes = models.TextField( help_text="Any additional information worth to mention.", blank=True )
    # sample roots
    sample_parent = models.ForeignKey( "self", verbose_name="Sample parent", help_text="Parent primary cell or cell line to bundle related cells.", null=True, blank=True ) # related_name="Source Name Parnet"
    def __str__( self ): 
        return( self.sample_name.annot_id )

    class Meta:
        ordering = ["sample_name"]


# sample detail level
class SampleDetailLevelManager( models.Manager): 
    def get_by_natural_key( self, sample_basiclevel, sample_detaillevel_serial ):
        return self.get( sample_basiclevel=sample_basiclevel, sample_detaillevel_serial=sample_detaillevel_serial )

class  SampleDetailLevel( models.Model ):
    # source provider
    objects = SampleDetailLevelManager()
    sample_basiclevel = models.ForeignKey( SampleBasicLevel, default="not_yet_specified", help_text="Choose sample." )  # SDTM : Source Name [USUBJID]
    sample_detaillevel_serial = models.SlugField( default="not_yet_specified", verbose_name="Sample detail level serial number", help_text="Unique serial number for this sample. Only alphanumeric keys and underscore allowed." )
    sample_provider = models.ForeignKey( Provider, default="not_yet_specified", help_text="Choose provider." ) # SDTM : Provider[STUDYID]
    sample_provider_catalog_id = models.CharField(  default="not_yet_specified", verbose_name="Provider catalog id", max_length=255, help_text="ID or catalogue number or name assigned to the cell by the vendor or provider." ) # characteristicSDTM : Sample Name [IDVAR=LBREFID],  # bue20141129: stays over years the catalog id the same? 
    sample_provider_batch_id = models.CharField(  default="not_yet_specified", verbose_name="Provider batch id", max_length=255, help_text="Batch or lot number assigned to the cell by the vendor or provider." )  # characteristic
    def __str__( self ): 
        s_out =  self.sample_basiclevel.sample_name.annot_id+'-'+self.sample_detaillevel_serial
        return( s_out )
    def natural_key( self ):
        return( self.sample_basiclevel.pk, self.sample_detaillevel_serial )
    natural_key.dependencies = ["app0sample.SampleBasicLevel"]

    class Meta: 
        unique_together = (("sample_basiclevel", "sample_detaillevel_serial"),)
        ordering = ["sample_basiclevel","sample_detaillevel_serial"]


# sample lab level 
class SampleLabLevelManager( models.Manager ): 
    def get_by_natural_key( self, sample_detaillevel__sample_basiclevel ,sample_detaillevel__sample_detaillevel_serial, sample_lablevel_serial ): 
        return self.get( sample_detaillevel__sample_basiclevel=sample_detaillevel__sample_basiclevel, sample_detaillevel__sample_detaillevel_serial=sample_detaillevel__sample_detaillevel_serial, sample_lablevel_serial=sample_lablevel_serial ) 

class SampleLabLevel( models.Model ): 
    # source lab
    objects = SampleLabLevelManager()
    sample_detaillevel = models.ForeignKey( SampleDetailLevel, help_text="Choose sample." )  # bue 20150225: sorry with natural keys i dont get a proper default running 
    sample_lablevel_serial = models.SlugField( default="not_yet_specified", verbose_name="Sample lab level serial number", help_text="Unique serial number for this sample. Only alphanumeric keys and underscore allowed." )    
    sample_verification_profile = models.ManyToManyField( VerificationProfile, verbose_name="Verification profile", help_text="Information pertaining to experimental verification of the primary cell identity STR profile." )  # characteristic
    sample_transient_modification = models.ManyToManyField( TransientModification, verbose_name="Transient modification", help_text="Transient transfection or viral transduction." )  # characteristic
    sample_lablevel_notes = models.TextField( verbose_name="Notes", help_text="Additional sample lab level related notes", blank=True )
    sample_lablevel_responsible = models.ForeignKey( Person, help_text="The person who did the sample_verification_profile or transient modification respective" )  # bue 20150225: sorry natural key
    sample_lablevel_date = models.DateField( default="1955-11-05" )
    def __str__( self ):
        s_out = self.sample_detaillevel.sample_basiclevel.sample_name.annot_id+'-'+self.sample_detaillevel.sample_detaillevel_serial+'-'+self.sample_lablevel_serial
        return( s_out)
    def natural_key( self ):
        return( self.sample_detaillevel.natural_key()+(self.sample_lablevel_serial,) )
    natural_key.dependencies = ["app0sample.SampleDetailLevel"]

    class Meta:
        unique_together = (("sample_detaillevel","sample_lablevel_serial"),)
        ordering = ["sample_detaillevel","sample_lablevel_serial"]



# single sample assay bridge 
class SampleSingleManager( models.Manager ): 
    def get_by_natural_key( self, sample_lablevel__sample_detaillevel__sample_basiclevel, sample_lablevel__sample_detaillevel__sample_detaillevel_serial, sample_lablevel__sample_lablevel_serial, sample_passage, sample_concentartion_unit, sample_concentration_value ): 
        return self.get( sample_lablevel__sample_detaillevel__sample_basiclevel=sample_lablevel__sample_detaillevel__sample_basiclevel, sample_lablevel__sample_detaillevel__sample_detaillevel_serial=sample_lablevel__sample_detaillevel__sample_detaillevel_serial, sample_lablevel__sample_lablevel_serial=sample_lablevel__sample_lablevel_serial, sample_passage=sample_passage, sample_concentartion_unit=sample_concentartion_unit, sample_concentration_value=sample_concentration_value )

class  SampleSingle( models.Model ):  
    # source lab
    objects = SampleSingleManager()
    sample_lablevel = models.ForeignKey( SampleLabLevel, help_text="Choose sample.", )  # bue 20150225: sorry natural key 
    sample_passage = models.SlugField( default="not_yet_specified", max_length=255, verbose_name="Passage", help_text="Write down passage number. Only alphanumeric keys and underscore allowed." )  # bue 20150213: this is characteristic and sample_lab_serial (Uniqe serial number for this sample on lab level)  
    sample_concentartion_unit = models.ForeignKey( Unit, default="not_yet_specified", verbose_name="Sample concentration unit", help_text="Value unit." )
    sample_concentration_value = models.FloatField( default=None, verbose_name="Sample concentration value", help_text="Value.", null=True, blank=True )
    #sample_notes = models.TextField( verbose_name="Notes", help_text="Additional sample lab level related notes", blank=True )
    #sample_culture_conditions_real = models.URLField( verbose_name="Actual culture conditions URL", help_text="Link to description of the actual tissue culture protocol (media, supplements, culture dish treatment) used to maintain the cell." )  # characteristic bue 20141119: url or protocol? bue 20150210: handles on protocol level
    def __str__( self ): 
        s_out = self.sample_lablevel.sample_detaillevel.sample_basiclevel.sample_name.annot_id+'-'+self.sample_lablevel.sample_detaillevel.sample_detaillevel_serial+'-'+self.sample_lablevel.sample_lablevel_serial+'-'+self.sample_passage+'|'+str(self.sample_concentartion_unit)+'-'+str(self.sample_concentration_value)
        return( s_out )
    def natural_key( self ):
        return( self.sample_lablevel.natural_key()+(self.sample_passage, self.sample_concentartion_unit.pk, self.sample_concentration_value) )
    natural_key.dependencies = ["app0sample.SampleLabLevel","apponunit_bioontology.Unit"]

    class Meta: 
        unique_together = (("sample_lablevel","sample_passage","sample_concentartion_unit","sample_concentration_value"),)
        ordering = ["sample_lablevel","sample_passage","sample_concentartion_unit","sample_concentration_value"]


#  sample set assay bridge 
# sample set track concentraton and passage in assay seperatly e.g. in saple layout file 
class SampleSet( models.Model ):
    sampleset_name = models.ForeignKey( SampleSetName, primary_key=True, default="not_yet_specified", help_text="Set a sampleset name." )
    sample_lablevel = models.ManyToManyField( SampleLabLevel, help_text="Choose samples which are become a member of the sample set.")
    def __str__( self ):
        return( self.sampleset_name.annot_id)

    class Meta:
        ordering = ["sampleset_name"]
