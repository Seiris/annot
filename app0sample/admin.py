from django.contrib import admin
from app0sample.models import SampleBasicLevel, SampleDetailLevel, SampleLabLevel, SampleSingle, SampleSet

# Register your models here.

# sample
#admin.site.register( SampleBasicLevel )
class SampleBasicLevelAdmin( admin.ModelAdmin ):
    fieldsets = [
        # sample name
        ("Sample name", { "fields":["sample_name","sample_lincs_name","sample_lincs_identifier"] } ),
        # biology features
        ("Biology features", { "fields":["sample_material_type","sample_organism","sample_organism_sex","sample_organism_ethnicity","sample_organism_age_year","sample_health_status","sample_disease","sample_organ","sample_tissue","sample_cell_type","sample_growth_propertie"] } ),
        # biological identification
        ("Biological identification", { "fields":["sample_cell_marker","sample_verification_profile_reference","sample_mutation_reference","sample_mutation_explicite"] } ),
        # biological engineering
        ("Biological engineering", { "fields":["sample_genetic_modification"] } ),
        # literature
        ("Litrature", { "fields":["sample_original_publication","sample_culture_conditions_recommended","sample_notes"] } ),
        # sample roots
        ("Roots", { "fields":["sample_parent"] } ),
     ]
    list_display = ("sample_parent","sample_name","sample_lincs_name","sample_lincs_identifier","sample_material_type","sample_organism","sample_organism_sex","sample_organism_age_year","sample_health_status","sample_organ","sample_tissue","sample_cell_type","sample_verification_profile_reference","sample_mutation_reference","sample_original_publication","sample_culture_conditions_recommended")
    search_fields = ("sample_parent","sample_name","sample_material_type","sample_organism","sample_organism_sex","sample_organism_ethnicity","sample_health_status","sample_disease","sample_organ","sample_tissue","sample_cell_type","sample_growth_propertie","sample_cell_marker","sample_mutation_explicite","sample_genetic_modification","sample_original_publication")
    filter_horizontal = ("sample_organism_ethnicity","sample_disease","sample_growth_propertie","sample_cell_marker","sample_mutation_explicite","sample_genetic_modification")
admin.site.register( SampleBasicLevel, SampleBasicLevelAdmin )

# sample detail level (provider) 
#admin.site.register( SampleDetailLevel )
class SampleDetailLevelAdmin( admin.ModelAdmin ):
    list_display = ("sample_basiclevel","sample_detaillevel_serial","sample_provider","sample_provider_catalog_id","sample_provider_batch_id")
    search_fields = ("sample_basiclevel","sample_provider") 
admin.site.register( SampleDetailLevel, SampleDetailLevelAdmin )

# sample lab level
#admin.site.register( SampleLabLevel )
class SampleLabLevelAdmin( admin.ModelAdmin ):
    list_display = ("sample_detaillevel","sample_lablevel_serial","sample_lablevel_notes","sample_lablevel_responsible","sample_lablevel_date")
    search_fields = ("sample_detaillevel","sample_lablevel_serial","sample_verification_profile","sample_transient_modification","sample_lablevel_notes","sample_lablevel_responsible")
    filter_horizontal = ("sample_verification_profile","sample_transient_modification")
admin.site.register( SampleLabLevel, SampleLabLevelAdmin )

# singe sample 
#admin.site.register( SampleSingle )
class SampleSingleAdmin( admin.ModelAdmin ):
    list_display = ("sample_lablevel","sample_passage","sample_concentartion_unit","sample_concentration_value")
    search_fields = ("sample_lablevel","sample_passage","sample_concentartion_unit")
admin.site.register( SampleSingle, SampleSingleAdmin )

# sample set
#admin.site.register( SampleSet )
class SampleSetAdmin( admin.ModelAdmin ):
    list_display = ("sampleset_name",)
    search_field = ("sampleset_name", "sample_lablevel")
    filter_horizontal = ("sample_lablevel",)
admin.site.register( SampleSet, SampleSetAdmin )
