from django.contrib import admin
from app0publication.models import Publication

# Register your models here.
#admin.site.register( Publication )
class PublicationAdmin( admin.ModelAdmin ): 
    list_display = ('pubmed_id','publication_doi','publication_author_list','publication_title','publication_status')
    search_fields = ('pubmed_id','publication_doi','publication_author_list','publication_title')
admin.site.register( Publication, PublicationAdmin )
