from django.db import models
# import ontologies 
from apponpublicationstatus_bioontology.models import PublicationStatus
# import bridges


# Create your models here.
class Publication( models.Model ):
    pubmed_id = models.PositiveIntegerField( help_text="PubMed ID of the described publication associated with this investigation (where available).", blank=True, null=True )
    publication_doi = models.CharField( max_length=255, help_text="A Digital Object Identifier (DOI) for that publication (where available).", blank=True )
    publication_author_list = models.TextField( default="not_yet_specified", help_text="The list of authors associated with that publication." )
    publication_title = models.CharField( max_length=255, primary_key=True, default="not_yet_specified", help_text="The title of publication associated with the investigation.")
    publication_status = models.ForeignKey( PublicationStatus, default="not_yet_specified", help_text="A term describing the status of that publication." ) 
    def __str__( self ):
        return( self.publication_title )

    class Meta: 
        ordering = ["publication_title"]
