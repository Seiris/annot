from django.contrib import admin
from appas_mema.models import SourcePlateLayout, SourcePlateLayoutBarecodeLevel, AssayMema, ZoomStudyAssayMema, ZoomResultProtocolRefSet


# Register your models here.

#admin.site.register( SourcePlateLayout ) 
class SourcePlateLayoutAdmin( admin.ModelAdmin ): 
    list_display = ('a_source_plate_layout_name','a_source_plate_layout_file')
    search_fields = ('a_source_plate_layout_name','a_source_plate_layout_file')
admin.site.register( SourcePlateLayout, SourcePlateLayoutAdmin )


#admin.site.register( SourcePlateLayoutBarecodeLevel ) 
class SourcePlateLayoutBarecodeLevelAdmin( admin.ModelAdmin ):
    list_display = ('a_source_plate_barcode','a_source_plate_layout','a_source_plate_factorset','a_source_plate_sop')
    search_fields = ('a_source_plate_barcode','a_source_plate_layout','a_source_plate_factorset','a_source_plate_sop')
admin.site.register( SourcePlateLayoutBarecodeLevel, SourcePlateLayoutBarecodeLevelAdmin ) 



#admin.site.register( ZoomResultProtocolRefSet )
class ZoomResultProtocolRefSetAdmin( admin.ModelAdmin ):
    list_display = ('assay','result_protocol_ref')
    search_fields = ('assay','result_protocol_ref')
admin.site.register( ZoomResultProtocolRefSet, ZoomResultProtocolRefSetAdmin )

class ZoomResultProtocolRefSetInline( admin.StackedInline ):
    model = ZoomResultProtocolRefSet
    extra = 0


#admin.site.register( AssayMema )
class AssayMemaAdmin( admin.ModelAdmin ):
    fieldsets = [
# run
        ('Run', { 'fields' : ['a_run_notes','a_run_barcode','a_run_name','a_run_plate_type'] } ), 
# source plate
        ('ECM reagent preparation', {'fields' : ['a_source_plate'] } ), 
# mema plate
        ('ECM printing', {'fields' : ['a_galfile','a_insoluble_factor_printing_sop'] } ),
# treatment
        ('Sample plating', { 'fields' : ['a_sample','a_sample_plating_sop'] } ),  # a_sample_layout_file only one sample per mema ecm set, 4 sample by 4 well plate possible 
        ('Soluble factor', {'fields' : ['a_soluble_factorset','a_soluble_factor_layout_file','a_soluble_factor_treatment_sop'] } ),
        #('Drug treatment', {'fields' : ['a_drug_factorset','a_drug_factor_layoutfile','a_drug_factor_treatment_sop'] } ), 
# measurement
        ('Staining', {'fields' : ['a_staining_parameterset','a_stainingset_layoutfile','a_staining_sop'] } ), 
        ('Image acquisition by scanning', {'fields' : ['a_image_acquisition_resop'] } ), 
        ('Image raw data feature extraction', {'fields' : ['a_image_feature_extraction_resop'] } ), 
    ]
#    list_display = ('a_name',)
#    search_fields = ('a_name',)
    filter_horizontal = ('a_source_plate', 'a_sample')
    inlines = [ZoomResultProtocolRefSetInline]   # source plate, sample 
admin.site.register( AssayMema, AssayMemaAdmin )


#admin.site.register( ZoomStudyAssayMema )
class ZoomStudyAssayMemaAdmin( admin.ModelAdmin ):
    list_display = ('study','assay')
    search_fields = ('study','assay')
admin.site.register( ZoomStudyAssayMema, ZoomStudyAssayMemaAdmin )
