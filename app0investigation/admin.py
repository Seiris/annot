from django.contrib import admin
from app0investigation.models import Investigation, ZoomInvestigationPublication, ZoomInvestigationPerson, ZoomInvestigationStudy


# Register your models here.
# publication 
#admin.site.register( ZoomInvestigationPublication )
class ZoomInvestigationPublicationAdmin(  admin.ModelAdmin ):
    list_display = ("investigation","publication")
    search_fields = ("investigation","publication")
admin.site.register( ZoomInvestigationPublication, ZoomInvestigationPublicationAdmin )

class ZoomInvestigationPublicationInline( admin.StackedInline ):
    model = ZoomInvestigationPublication
    extra = 0


# person
#admin.site.register( ZoomInvestigationPerson )
class ZoomInvestigationPersonAdmin(  admin.ModelAdmin ):
    list_display = ("investigation","person")
    search_fields = ("investigation","person","investigation_person_role")
    filter_horizontal = ("investigation_person_role", )
admin.site.register( ZoomInvestigationPerson, ZoomInvestigationPersonAdmin )
 
class ZoomInvestigationPersonInline( admin.StackedInline ):
    model = ZoomInvestigationPerson
    filter_horizontal = ("investigation_person_role", )
    extra = 0


# study 
#admin.site.register( ZoomInvestigationStudy )
class ZoomInvestigationStudyAdmin( admin.ModelAdmin ):
    list_display = ("investigation","study")
    search_fields = ("investigation","study")
admin.site.register( ZoomInvestigationStudy, ZoomInvestigationStudyAdmin )

class ZoomInvestigationStudyInline( admin.StackedInline ):
    model = ZoomInvestigationStudy
    extra = 1


#admin.site.register( Investigation )
class InvestigationAdmin( admin.ModelAdmin ): 
    list_display = ("i_identifier","i_title","i_description","i_submission_date","i_public_release_date")
    search_fields = ("i_identifier","i_title","i_description")
    inlines = [ZoomInvestigationPublicationInline, ZoomInvestigationPersonInline, ZoomInvestigationStudyInline]
admin.site.register( Investigation, InvestigationAdmin )
