from django.db import models
# import ontologies
from apponpersonrole_own.models import PersonRole
# import bridges
from app0person.models import Person
from app0publication.models import Publication
from app0study.models import Study

# Create your models here.
# investigation model
class Investigation( models.Model ): 

    # INVESTIGATION
    i_identifier = models.SlugField( primary_key=True, default="not_yet_specified", verbose_name="Investigation identifier", help_text="A unique identifier." )
    i_title = models.CharField ( default="not_yet_specified", max_length=255, verbose_name="Investigation title", help_text="A concise name given to the investigation." )
    i_description = models.TextField( default="not_yet_specified", verbose_name="Investigation description", help_text="A textual description of the investigation." )
    i_submission_date = models.DateField( default="1955-11-05", verbose_name="Investigation submission date", help_text="The date on which the investigation was reported to the repository." )
    i_public_release_date = models.DateField( default="1955-11-05", verbose_name="Investigation public release date", help_text="The date on which the investigation should be released publicly." )

    # INVESTIGATION PUBLICATIONS
    # goto admin and appbripublication
   
    # INVESTIGATION CONTACTS
    # goto admin and appbriperson
    def __str__( self ):
        return( self.i_identifier )

    class Meta:
        ordering = ["i_identifier"]


# investigation publication bridge 
class ZoomInvestigationPublicationManager( models.Manager ):
    def get_by_natural_key( self, investigation, publication ):
        return self.get( investigation=investigation, publication=publication )

class ZoomInvestigationPublication( models.Model ):
    objects = ZoomInvestigationPublicationManager() 
    investigation = models.ForeignKey ( Investigation, default="not_yet_specified", help_text="Inverstigation." )
    publication = models.ForeignKey( Publication, default="not_yet_specified", help_text="Publication associated with this investigation." ) 
    def __str__( self ):
        s_out = self.investigation.i_identifier+'-'+self.publication.publication_title
        return( s_out )
    def natural_key( self ): 
        return( self.investigation.pk, self.publication.pk )
    natural_key.dependencies = ["app0investigation.Investigation","app0publication.Publication"]

    class Meta: 
        unique_together = (("investigation","publication"),)
        ordering = ["investigation","publication"]


# investigation persion bridge 
class ZoomInvestigationPersonManager( models.Manager ):
    def get_by_natural_key( self, investigation, person ):
        return self.get( investigation=investigation, person=person )

class ZoomInvestigationPerson( models.Model ): 
    objects = ZoomInvestigationPersonManager()
    investigation = models.ForeignKey ( Investigation, default="not_yet_specified", help_text="Inverstigation." )
    person = models.ForeignKey( Person, help_text="Person associated with the investigation." ) 
    investigation_person_role = models.ManyToManyField( PersonRole, help_text="Term to classify the role(s) performed by this person in the context of the investigation, which means that the roles reported here need not correspond to roles held withing their affiliated organization. Multiple annotations or values attached to one person can be provided by using a semicolon as a separator (e.g.: submitter;funder;sponsor)." ) 
    def __str__( self ):
        s_out = self.investigation.i_identifier+'-'+self.person.person_first_name+'_'+self.person.person_mid_initials+'_'+self.person.person_last_name
        return( s_out ) 
    def natural_key( self ): 
        return( self.investigation.pk, self.person.pk )
    natural_key.dependencies = ["app0investigation.Investigation","app0person.Person"]

    class Meta:
        unique_together = (("investigation","person"),)
        ordering = ["investigation","person"]


# investigation study bridge
class ZoomInvestigationStudyManager( models.Manager ):
    def get_by_natural_key( self, investigation, study ):
        return self.get( investigation=investigation, study=study )

class ZoomInvestigationStudy( models.Model ): 
    objects = ZoomInvestigationStudyManager()
    investigation = models.ForeignKey ( Investigation, default="not_yet_specified", help_text="Inverstigation." )
    study = models.ForeignKey( Study, default="not_yet_specified", help_text="Study associated with this investigation." ) 
    def __str__( self ):
        s_out = self.investigation.i_identifier+"-"+self.study.s_identifier
        return( s_out)
    def natural_key( self ): 
        return( self.investigation.pk, self.study.pk )
    natural_key.dependencies = ["app0investigation.Investigation","app0study.Study"]

    class Meta: 
        unique_together = (("investigation","study"),)
        ordering = ["investigation","study"]
