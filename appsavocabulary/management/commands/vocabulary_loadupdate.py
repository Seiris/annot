# import form django 
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import python
import datetime 
import glob
import importlib
import inspect
import json

# import annot
from appsavocabulary.models import SysAdminVocabulary
#from prjannot.settings import BASE_DIR

class Command( BaseCommand ):
    args = "<apponmmm_source apponmmm_source ...>"
    help = "Upload vocabulary updates."

    def handle(self, *args, **options): 
        # initiate 
        queryset = SysAdminVocabulary.objects.all()  # get queryset
        ls_queryset = []
        ls_apponfilter = []

        # load filter_vocabulary_getupdate.txt
        self.stdout.write("Load vocabulary_getupdate filter" )
        with open( "appsavocabulary/filter_vocabulary.txt", 'r' ) as f:
            for s_line in f:
                s_line = s_line.strip()
                if ( s_line != "" ):
                    ls_apponfilter.append(s_line)
            self.stdout.write( "Filter: " + str(ls_apponfilter) )

        # treat argument specified vocabulary
        if ( len(args) > 0 ): 
            # get ontologies app 
            ls_appon = list( args )
            for obj_n in queryset:
                # check if vocabulary app in vocabulary table exist  
                if ( str(obj_n.app) in ls_appon):
                    ls_queryset.append( str(obj_n.app) )  # populate queryset list 
        # treat all ontologies
        else:
            # get ontologies app 
            ls_appon = glob.glob('appon*') 
            ls_appon.sort()
            # reset queryset 
            self.stdout.write("Reset vocabulary  table" )
            for obj_n in queryset: 
                # check if vocabulary app in vocabulary table exist  
                if ( str(obj_n.app) in ls_appon):
                    ls_queryset.append( str(obj_n.app) )  # populate queryset list 
                else:
                    obj_n.delete()  # delete vocabulary out of table 

        # process vocabulary app
        for s_appon in ls_appon:
            # python manage.py vocabulary_getupdate s_appon
            management.call_command( "vocabulary_getupdate", s_appon, )

            self.stdout.write( "\nProcessing vocabulary database upload: "+s_appon )
            # check process filter
            if ( s_appon in ls_apponfilter ):
                # don't process this vocabulary
                self.stdout.write( "WARNING: vocabulary update will not be loaded because it is listed in appsavocabulary/filter_vocabulary.txt: "+s_appon )
                
            else:                    
                # load latest database version  
                try:
                    m_appon = importlib.import_module( s_appon+'.models' ) 
                    for s_name, obj in inspect.getmembers( m_appon ):
                        if inspect.isclass( obj ):
                            s_apponmodels = s_name
                            c_apponmodels = obj  # eqivalent to json model 
                            lo_record = c_apponmodels.objects.all( )
                            self.stdout.write( "Load database vocabulary: "+s_apponmodels )

                except ImportError: 
                    raise CommandError( "Annot vocabulary app %s does not exist." % s_appon )


                # load related files 
                obj_n = SysAdminVocabulary.objects.get( app=s_appon )
                s_vocabulary = obj_n.vocabulary

                # load term filter if existent
                ls_fterm = []
                s_ffterm = s_appon+"/filter_"+s_vocabulary+"_term.txt"
                try: 
                    self.stdout.write( "Load term filter file: "+s_ffterm )
                    f_fterm = open( s_ffterm, 'r' )  # open file handle  
                    for s_line in f_fterm:
                        s_term = s_line.strip() 
                        ls_fterm.append( s_term )
                    f_fterm.close()
                except: 
                    self.stdout.write( "No trem filter file found: "+s_ffterm )

                # load indentifier filter if existent
                ls_ffier = []
                s_fffier = s_appon+"/filter_"+s_vocabulary+"_identifier.txt"
                try: 
                    self.stdout.write( "Load identifier filter file: "+s_fffier )
                    f_ffier = open( s_fffier, 'r' )  # open file handle  
                    for s_line in f_ffier:
                        s_fier = s_line.strip() 
                        ls_ffier.append( s_fier )
                    f_ffier.close()
                except: 
                    self.stdout.write( "No identifier filter file found: "+s_fffier )

                # load latest json file version 
                i_termcount = 0
                s_version_latest =  obj_n.version_latest
                o_version_latest_date = obj_n.version_latest_date
                s_version_latest_date = str( o_version_latest_date )
                s_version_latest_date = s_version_latest_date.replace( '-','' )
                s_filefixture = s_appon+"/fixtures/"+s_version_latest_date+'_'+s_vocabulary+'_'+s_version_latest+"_original.json"
                f_fixture = open( s_filefixture, 'r' )  # open file handle  
                self.stdout.write( "Load latest json file version: "+s_filefixture )
                try:  
                    # if file not empty 
                    ld_json = json.load( f_fixture )   
                    i_termcount = len( ld_json )
                    # pack list of dictionary into dictionary of dictionary where the term_name is the key
                    dd_json = {}
                    for d_json in ld_json :
                         s_term_file = d_json["fields"]["term_name"]
                         dd_json.update( {s_term_file:d_json} )
                    # update filter 
                    if ( (len(ls_fterm) == 0) and (len(ls_ffier) == 0) ): 
                        ls_fterm = list( dd_json.keys() ) 

                    # loop through loaded database version
                    self.stdout.write( "Update database version term status." )
                    for o_record in lo_record :
                        s_term_record = o_record.term_name
                        # check every vocabulary term if it is a member of the latest json file version
                        try:
                            d_json = dd_json[s_term_record]
                            #o_record.term_name  
                            o_record.term_id = d_json["fields"]["term_id"]
                            #o_record.annot_id is equivalent to json pk or pwn
                            o_record.term_source_version_responsible = "term_from_original_source"
                            s_date = d_json["fields"]["term_source_version_update"]
                            ls_date = s_date.split('-')
                            i_year = int(ls_date[0])
                            i_month = int(ls_date[1])
                            i_day = int(ls_date[2])
                            o_record.term_source_version_update = datetime.date( i_year,i_month,i_day )
                            o_record.term_source_version = d_json["fields"]["term_source_version"] 
                            o_record.term_ok = True  # set status  
                            o_record.save()
                        except KeyError: 
                            o_record.term_ok = False  # reset status
                            o_record.save()

                    # loop through json file version 
                    self.stdout.write( "Load latest json file version into the database.")
                    for d_json in ld_json :
                        s_term_name = d_json ["fields"]["term_name"]
                        s_term_id=d_json["fields"]["term_id"]
                        #print( "Prefilter name and identifier", s_term_name, s_term_id )
                        # check if json file entry term passes the filters
                        if ( ( s_term_name in ls_fterm ) or ( s_term_id in ls_ffier ) ):
                            try: 
                                # check if json file entry term allredy in database
                                c_apponmodels.objects.get( term_name=s_term_name)
                            except c_apponmodels.DoesNotExist:
                                # write missing json file entry into database 
                                s_date = d_json["fields"]["term_source_version_update"]
                                ls_date = s_date.split('-')
                                i_year = int(ls_date[0])
                                i_month = int(ls_date[1])
                                i_day = int(ls_date[2])
                                o_newrecord = c_apponmodels( term_name=s_term_name, term_id=s_term_id, annot_id=d_json["pk"], term_source_version_responsible="term_from_original_source", term_source_version_update=datetime.date( i_year,i_month,i_day ), term_source_version= d_json["fields"]["term_source_version"], term_ok=True )
                                o_newrecord.save()

                except ValueError:
                    # empty vocabulary file
                    self.stdout.write( "WARNING: onology file is empty. This can happen if this is a completly new own vocabulary with yet no a single terms defined.") 

                # laod not_yet_specified and not_specified term
                self.stdout.write( "Load not_yet_specified and not_specified term.") 
                try: 
                    # check if json file entry term allredy in database
                    c_apponmodels.objects.get( term_name="not available")
                except c_apponmodels.DoesNotExist:
                    # write missing json file entry into database
                    o_newrecord = c_apponmodels( term_name="not available", term_id="not_available", annot_id="not_available", term_source_version_responsible="annot_basic_term" )
                    o_newrecord.save()
                try: 
                    # check if json file entry term allredy in database
                    c_apponmodels.objects.get( term_name="not yet specified")
                except c_apponmodels.DoesNotExist:
                    # write missing json file entry into database
                    o_newrecord = c_apponmodels( term_name="not yet specified", term_id="not_yet_specified", annot_id="not_yet_specified", term_source_version_responsible="annot_basic_term" )
                    o_newrecord.save()



                # update vocabulary status
                self.stdout.write("Update vocabulary status" )
                obj_n = SysAdminVocabulary.objects.get( app=s_appon )
                #obj_n.app 
                #obj_n.vocabulary
                #obj_n.ontology
                #obj_n.url
                #obj_n.version_latest # downloaed from the online source 
                #obj_n.version_latest_date  # downloaed from the online source 
                obj_n.version_loaded  =  s_version_latest # uploaed into annot database 
                obj_n.version_loaded_date  = o_version_latest_date  # uploaed into annot database  
                obj_n.original_term_count = i_termcount
                obj_n.up_to_date = obj_n.version_loaded_date >= obj_n.version_latest_date  # uploaed into annot database 
                obj_n.save()
