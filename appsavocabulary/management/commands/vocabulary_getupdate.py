# import django 
from django.core.management.base import BaseCommand, CommandError
# import python
import csv
import datetime
import glob
import importlib
import json
import os
import re
import sys
import time
import urllib.parse
import urllib.request
# import annot
from prjannot import crowbar  # annot passkey library
from prjannot.settings import BASE_DIR
from prjannot.settings import MEDIA_ROOT
from appsavocabulary.models import SysAdminVocabulary



### internal functions ###
def pknamingconvention( s_term, s_identifier, b_lowerpk=True ):
    """
    internal methode
    """
    # manipulate overflow term
    if ( len(s_term) > 128 ):
        s_term = "longterm"
    # manipulate identifier
    s_ident =  re.sub( r'[^a-zA-Z0-9]','', s_identifier ) 
    # build annot id primary key
    s_pk = s_term+'_'+s_ident
    s_pk = re.sub( r'[^a-zA-Z0-9]','_', s_pk )
    s_pk = re.sub( r'_+','_', s_pk )
    if b_lowerpk : 
        s_pk = s_pk.lower()
    # output
    return( s_pk )


def appendrecord( ld_json, s_name, s_id, s_pk, s_release, s_version, s_model, b_swaptermid=False ):
    """
    internal methode
    """
    d_record = {}
    d_fields = {}
    # field 
    if ( b_swaptermid ): 
        d_fields.update( {"term_name":s_id} )
        d_fields.update( {"term_id":s_name} )
    else:
        d_fields.update( {"term_name":s_name} )
        d_fields.update( {"term_id":s_id} )
    d_fields.update( {"term_source_version_responsible":"term_from_original_source"} )
    d_fields.update( {"term_source_version_update":s_release} )
    d_fields.update( {"term_source_version":s_version} )
    d_fields.update( {"term_ok":True} )
    # record
    d_record.update( {"fields":d_fields} )
    d_record.update( {"model":s_model} )
    d_record.update( {"pk":s_pk} )
    # genome
    ld_json.append( d_record )
    # output
    return( ld_json )


### ontology file formats ###
# obo format; chebi
def obo2jsonontology( s_ifile, s_ofile, s_model, s_release, s_version, b_swaptermid=False, b_lowerpk=True ): 
    """"
    get called from vocabulary_getupdate
    """
    # set empty 
    ld_out = []
    ds_record = {}
    b_term = False
    b_record = False
    b_maybeobsolet = False
    # read out file
    with open( s_ifile, 'r' ) as f:
        for s_line in f: 
            s_entry = s_line.strip()
            # set term and record flags
            if ( re.search( r"^\[Term]", s_entry ) != None ):
                b_term = True
                b_maybeobsolet = False
                # erase record 
                s_term = None
                s_identifier = None
                s_pk = None
            elif ( re.search( r"^is_obsolete.*:.*true", s_entry) != None ):  # obsolet term record 
                if b_maybeobsolet :
                    ld_out.pop(-1)
                b_term = False
                b_maybeobsolet = False
            elif ( len(s_entry) == 0 ):  # end term record 
                b_term = False
                b_maybeobsolet = False


            # if term flag true
            if b_term : 
                # load record
                ls_entry = s_entry.split(': ')
                # get term
                if ( ls_entry[0].strip() == "name" ):
                    if ( len(ls_entry) > 2 ): 
                        print("Warning : name entry contains more then a key:value pair. Detected term migth be incomplet.", ls_entry[1], ls_entry)
                    s_term = ls_entry[1].strip()
                # get identifier 
                elif ( ls_entry[0].strip() == "id" ): 
                    if ( len(ls_entry) > 2 ): 
                        print("Warning : identifier entry contains more then a key:value pair. Detected term migth be incomplet.", ls_entry[1], ls_entry)
                    s_identifier = ls_entry[1].strip()
                # get nothing 
                else: 
                    pass
                # set record flag 
                if ( (s_term != None)&( s_identifier != None) ): 
                    b_record = True

            # put record 
            if b_record :  # & (b_term == False)
                # set record and term flag back 
                b_record = False
                # store record 
                s_pk = pknamingconvention( s_term=s_term, s_identifier=s_identifier, b_lowerpk=b_lowerpk )
                ld_out = appendrecord( ld_json=ld_out, s_name=s_term, s_id=s_identifier, s_pk=s_pk, s_release=s_release, s_version=s_version, s_model=s_model, b_swaptermid=b_swaptermid )
                # set obsolet and term flag 
                b_maybeobsolet = True
                b_term = False

    # close input file 
    f.closed
    # write to file 
    f_file = open( s_ofile, 'w' ) # open file handle
    json.dump( ld_out, f_file,  sort_keys=True, indent=4 )
    # return
    return()



# umls format;  ncbitaxon
def umls2jsonontology( s_ifile, s_ofile, s_model, s_release, s_version, b_swaptermid=False, b_lowerpk=True ): 
    """"
    get called from vocabulary_getupdate
    """
    # set empty 
    ld_out = []
    s_term = None
    s_identifier = None
    b_term = False    
    b_record = False
    # read out file
    with open( s_ifile, 'r' ) as f:
        for s_line in f: 
            s_entry = s_line.strip()
            # set term and record flags
            if ( re.search( r"^<http://purl.bioontology.org/ontology/NCBITAXON/[0-9]*> a owl:Class ;", s_entry ) != None ):
                b_term = True
                # erase record 
                s_term = None
                s_identifier = None
                s_pk = None
            elif ( re.search( r"^.*\.$", s_entry ) != None ): 
                b_term = False

            # if term flag true
            if b_term : 
                # load record
                ls_entry = s_entry.split('"""')
                # get term
                if ( ls_entry[0].strip() == "skos:prefLabel" ):
                    s_term = ls_entry[1].strip()
                # get identifier
                elif ( ls_entry[0].strip() == "skos:notation" ):
                    s_identifier = ls_entry[1].strip() 
            # get nothing 
            else: 
                pass 
            # set record flag 
            if ( (s_term != None) and ( s_identifier != None) ): 
                b_record = True

            # put record 
            if( b_record and not(b_term) ):
                # set b_record back 
                b_record = False
                # store record 
                s_pk = pknamingconvention( s_term=s_term, s_identifier=s_identifier, b_lowerpk=b_lowerpk )
                ld_out = appendrecord( ld_json=ld_out, s_name=s_term, s_id=s_identifier, s_pk=s_pk, s_release=s_release, s_version=s_version, s_model=s_model, b_swaptermid=b_swaptermid )
    # close imput file 
    f.closed
    # write to file 
    f_file = open( s_ofile, 'w' ) # open file handle
    json.dump( ld_out, f_file,  sort_keys=True, indent=4 )
    # return
    return()



# gtf format; ensembl gene 
def gtf2jsonontology( s_ifile, s_ofile, s_model, s_release, s_version, b_swaptermid=False, b_lowerpk=True ): 
    """
    get called from vocabulary_getupdate 
    """
    # empty output 
    ld_genome = []
    ds_genome = {}
    # processing
    with open( s_ifile, newline='') as f:
        reader = csv.reader(f, delimiter='\t')
        for ls_tuple in reader:
            #print( ls_tuple )
            # get row
            if ( len (ls_tuple) >= 9 ):
                # get input string  
                s_extract = ls_tuple[8]
                # get input string into list 
                ls_extract = s_extract.split(';')
                # get input string into dict
                ds_extract = {}
                for s_xtract in ls_extract: 
                    ls_fractal = s_xtract.split('"') 
                    if ( len(ls_fractal) > 1 ): 
                        ds_extract.update( {ls_fractal[0].strip():ls_fractal[1].strip()} )
                # update genome dict and json compatible genome list of dicts
                s_geneid = ds_extract["gene_id"]
                s_genename = ds_extract["gene_name"]
                try: 
                    s_found = ds_genome[s_geneid]
                    if not( s_found == s_genename ): 
                        sys.exit("Error: ensg gene id "+s_geneid+" owns more then one gene name "+s_found+" and "+s_genename+" .")
                except KeyError:
                     # update ds_genome
                     ds_genome[s_geneid] = s_genename
                     # update ld_genome
                     s_pk = pknamingconvention( s_term=s_genename, s_identifier=s_geneid, b_lowerpk=b_lowerpk )
                     ld_genome = appendrecord( ld_json=ld_genome, s_name=s_genename, s_id=s_geneid, s_pk=s_pk, s_release=s_release, s_version=s_version, s_model=s_model, b_swaptermid=b_swaptermid )
    # write to file 
    f_file = open( s_ofile, 'w' ) # open file handle
    json.dump( ld_genome, f_file,  sort_keys=True, indent=4 )
    # return
    return()



### http format ###
# http ensembl org rest api communication
def ensemblorg2get_latest( s_url, s_organism, s_pathmedia ):
    """
    get called from vocabulary_getupdate 
    """
    ds_latest = {}
    # request
    s_url = "http://rest.ensembl.org/info/species?content-type=application/json"
    o_request = urllib.request.Request( s_url )
    o_request.add_header( "Content-Type", "application/json")
    #print( "\nBUE request :",o_request )

    # response
    o_httpresponse = urllib.request.urlopen( o_request )
    #print( "\nBUE httpresponse :", o_httpresponse )

    # get header 
    o_header = o_httpresponse.info()
    s_header = str(o_header)
    #print( "\nBUE header :", s_header ) 

    # get content
    s_page = o_httpresponse.read().decode("utf-8")
    #print( "\nBUE page :", s_page , type(s_page) )

    # json hack
    d_page = json.loads( s_page )
    l_page = d_page["species"]
    # get s_assembly = "GRCh38"
    # get i_release = "78"
    for d_entry in l_page:
        print( "Check for organism :", d_entry["name"] )
        if ( d_entry["name"] == s_organism ):
            print("Found." )
            s_assembly = d_entry["assembly"]  # assembly 
            i_release =  d_entry["release"] # release
            break 
    # get version string
    s_version = str(i_release)+s_assembly 

    # wget 
    s_load = "*."+s_assembly+"."+str(i_release)+".gtf.gz"
    # wget ftp://ftp.ensembl.org/pub/release-78/gtf/homo_sapiens/Homo_sapiens.GRCh38.78.gtf.gz
    s_url = "ftp://ftp.ensembl.org/pub/release-"+str(i_release)+"/gtf/"+s_organism+"/"+s_load
    s_cwd = os.getcwd()
    os.chdir( s_pathmedia )
    os.system( "wget -c "+s_url )
    # find file name
    ls_file = glob.glob( s_load )
    if not( len( ls_file ) == 1 ):
        sys.exit("Error: more then one or non downloaded extracted ensembl gft file from the requested release found. Globed for "+s_load+" .")
    else: 
        # find release date 
        s_file = ls_file[0]
        s_date = time.strftime("%Y-%m-%d", time.gmtime(os.path.getmtime( s_file )))
        # pack output
        ds_latest["s_release"] = s_date 
        ds_latest["s_version"] = s_version
        ds_latest["s_file"] = s_pathmedia+s_file 
    # return
    os.chdir( s_cwd )
    return( ds_latest )



# http uniprot org rest api communication
def uniprotorg2get_httpresponse( s_url, s_query, s_contact ): 
    """
    get called from vocabulary_getupdate
    """
    # handle parameter
    d_params = {
        'query':s_query,
        'format':'tab',
        'columns':'organism,organism-id,genes,protein names,id,keywords'
        #'include':, 
        #'copress':,
        #'limit':,
        #'offset':, 
    }
    s_params = urllib.parse.urlencode( d_params )
    #print( str(s_params) )
    # build request
    o_request = urllib.request.Request( s_url+'?'+s_params )
    o_request.add_header( "User-Agent", "Python/3.x ( %s )" % s_contact )
    #print( o_request )
    # get response
    o_httpresponse = urllib.request.urlopen( o_request )
    #print( o_httpresponse )
    return( o_httpresponse )

    
def uniprotorg2extract_header( o_httpresponse ):
    """
    get called from vocabulary_getupdate
    """
    # empty output
    ds_info = {}
    # processing
    s_info = str( o_httpresponse.info() )
    ls_info = s_info.split('\n')
    for s_info in ls_info:
        #print( s_info )
        ls_entry = s_info.split(': ')
        if (len( ls_entry ) >= 2 ):
            ds_info.update( {ls_entry[0]:ls_entry[1]} )
    # fix version 
    s_version = ds_info['X-UniProt-Release']
    s_version = re.sub( r'[^a-zA-Z0-9]','-', s_version )
    ds_info['X-UniProt-Release'] = s_version
    # output
    return( ds_info )


def uniprotorg2extract_page( o_httpresponse, s_pathmedia ):
    """
    get called from vocabulary_getupdate
    """
    # get content
    s_page = o_httpresponse.read().decode("utf-8")
    # empty output
    lds_response = []
    # csv handling 
    s_file = s_pathmedia+"uniproteom_homosapines.csv"
    with open( s_file, 'w', newline='') as f: 
        writer = csv.writer(f)
        # handle header
        ls_row = s_page.split( '\n' )
        s_header = ls_row.pop(0)
        ls_header = s_header.split( '\t' )
        writer.writerow( ls_header )  # csv handling
        # handle row
        for s_row in ls_row:
            ds_response = {}
            ls_row = s_row.split( '\t' )
            writer.writerow( ls_row )  # csv handling
            if ( len( ls_row ) >= len(ls_header) ):
                i = 0
                for s_header in ls_header:
                    ds_response.update( {s_header:ls_row[i]} )
                    i += 1
                lds_response.append( ds_response )
    # output
    return( lds_response )


def uniprotorg2pack_proteom( lds_page, ds_header, s_model,  b_swaptermid=True, b_lowerpk=False): 
    """
    get called from vocabulary_getupdate
    """
    # empty output
    ld_vocabulary = []
    # get packed
    for ds_page in lds_page :
        # primary gene name
        s_genes = ds_page["Gene names"]
        ls_gene = s_genes.split(' ')
        if ( len(ls_gene) >= 1 ):
            s_gene = ls_gene[0]
            s_gene = s_gene.strip()
        else: 
            s_gene =''
        # protein identifier
        s_identifier = ds_page["Entry"]
        s_identifier = s_identifier.strip()
        # primary key
        s_pk = pknamingconvention( s_term=s_gene, s_identifier=s_identifier, b_lowerpk=b_lowerpk )
        # primery protein name 
        s_protein = ds_page["Protein names"]
        s_protein = s_protein.strip()
        #s_protein = s_protein+' '+s_identifier
        # version
        s_version = ds_header["X-UniProt-Release"]
        # version_date 
        s_versiondate = s_version+"-01"
        # store record 
        ld_vocabulary = appendrecord( ld_json=ld_vocabulary, s_name=s_protein, s_id=s_identifier, s_pk=s_pk, s_release=s_versiondate, s_version=s_version, s_model=s_model, b_swaptermid=b_swaptermid )
    # output 
    return( ld_vocabulary )



# http biomedical ontology org rest api communication 
# get from url whole ontology fle
def bioontologyorg2get_file( s_url, s_apikey, s_ofile):
    """"
    get called from vocabulary_getupdate 
    """
    opener = urllib.request.build_opener() 
    opener.addheaders = [("Authorization", "apikey token=" + s_apikey)] 
    s_record = opener.open( s_url ).read().decode( "utf-8" )  # type bytes turn into unicode
    f = open( s_ofile, 'w')
    f.write( s_record )
    f.close()
    return( s_ofile )


# get from url json
def bioontologyorg2get_json( s_url, s_apikey ): 
    """
    bioontologyorg internal method
    """
    opener = urllib.request.build_opener() 
    opener.addheaders = [("Authorization", "apikey token=" + s_apikey)] 
    s_record = opener.open( s_url ).read().decode( "utf-8" )  # type bytes turn into unicode
    d_record = json.loads( s_record )
    return( d_record ) 


# extract form json 
def bioontologyorg2extract_version( s_url, s_apikey ): 
    """
    get called from vocabulary_getupdate
    """
    # get json
    ds_extract = {}
    d_record = bioontologyorg2get_json( s_url=s_url, s_apikey=s_apikey )
    # extraction
    s_version = d_record["version"]
    s_version = re.sub( r'[^a-zA-Z0-9]','-', s_version )
    s_date = d_record["released"]
    s_date = s_date[0:10]
    #s_date = s_date.replace( '-','' )
    ds_extract.update( {"version":s_version} )
    ds_extract.update( {"date":s_date} )
    # output
    return( ds_extract )


def bioontologyorg2extract_term( d_record, d_extract ):
    """
    bioontology internal method
    """
    # put extract in normal form
    try: 
        l_record = d_record["collection"]
        s_urlnext = d_record["links"]["nextPage"]
    except( KeyError ):
        l_record = [d_record,]
        s_urlnext = None
    # extract info 
    for d_record in l_record :  
        s_identifier = d_record["@id"]
        ls_identifier = s_identifier.split('/')
        s_onidentifier = ls_identifier[-1]
        s_onterm = d_record["prefLabel"]
        # output 
        if ( (s_onidentifier != None)&(s_onterm != None) ): 
            #d_extract.update( {s_onterm:s_onidentifier} )
            d_extract.update( {s_onidentifier:s_onterm} )  # the identifier will be more unique as the term
    # return
    return( d_extract, s_urlnext )


# get whole branch 
def bioontologyorg2extract_branch( s_url, s_apikey, b_getbranchcut=True ):
    """
    get called from vocabulary_getupdate
    """
    d_branch = {}
    # get cut
    if ( b_getbranchcut ):
        d_record = bioontologyorg2get_json( s_url=s_url, s_apikey=s_apikey )
        ( d_branch, s_urlnop ) = bioontologyorg2extract_term( d_record=d_record, d_extract=d_branch )
    # get whole branch by iterate through the pages 
    s_urlnext = s_url+"/descendants"
    while not ( s_urlnext == None ): 
        d_record = bioontologyorg2get_json( s_url=s_urlnext, s_apikey=s_apikey )
        ( d_branch, s_urlnext ) = bioontologyorg2extract_term( d_record=d_record, d_extract=d_branch )
        #print( d_branch, s_urlnext )
    # output
    return( d_branch ) 


# pack branch and version django annot vocabulary json fixture compatible
def bioontologyorg2pack_branch( d_branch, d_version, s_model, b_swaptermid=False, b_lowerpk=True ):
    """
    get called from vocabulary_getupdate
    """
    # pack branch django fixture compatible
    ld_branch = []
    for s_key in d_branch: 
        d_entry = {}
        d_field = {}
        # build primary key 
        s_term = d_branch[s_key]
        s_pk = pknamingconvention( s_term=s_term, s_identifier=s_key, b_lowerpk=b_lowerpk )
        # store entry by populateing list of dictionary branch 
        ld_branch = appendrecord( ld_json=ld_branch, s_name=s_term, s_id=s_key, s_pk=s_pk, s_release=d_version["date"], s_version=d_version["version"], s_model=s_model, b_swaptermid=b_swaptermid )
    # output
    return( ld_branch )
 


### main ###
class Command( BaseCommand ):
    args = '<apponmmm_source apponmmm_source ...>'
    help = 'Check for vocabulary updates.'
 
    def handle( self, *args, **options ): 
        # initiate 
        os.chdir( BASE_DIR )  # set path 
        queryset = SysAdminVocabulary.objects.all()  # get queryset 
        ls_queryset = []
        ls_apponfilter = []

        # load filter_vocabulary_getupdate.txt
        self.stdout.write("Load vocabulary_getupdate filter" )
        with open( "appsavocabulary/filter_vocabulary.txt", 'r' ) as f:
            for s_line in f:
                s_line = s_line.strip()
                if ( s_line != "" ):
                    ls_apponfilter.append(s_line)
            self.stdout.write( "Filter: " + str(ls_apponfilter) )

        # treat argument specified vocabulary
        self.stdout.write("Handle vocabulary_getupdate args" )
        if ( len(args) > 0 ): 
            # get ontologies app 
            ls_appon = list( args )
            for obj_n in queryset:
                s_apponobj = str(obj_n.app)
                # check if vocabulary app in vocaulary table exist  
                if ( s_apponobj in ls_appon):
                    ls_queryset.append( s_apponobj )  # populate queryset list 
        # treat all ontologies for which a vocabulary app exist
        else:
            # get ontologies app 
            ls_appon = glob.glob('appon*') 
            ls_appon.sort()
            # reset vocabulary table 
            self.stdout.write("Reset vocabulary table" )
            for obj_n in queryset: 
                # check if vocabulary app in vocabulary table exist
                s_apponobj = str(obj_n.app)
                if ( s_apponobj in ls_appon):
                    ls_queryset.append( s_apponobj )  # populate queryset list
                else:
                    obj_n.delete()  # delete vocabulary out of table 

        # process vocabulary list
        for s_appon in ls_appon:
            self.stdout.write( "\nProcessing vocabulary get: "+s_appon )
            try: 
                m_appon = importlib.import_module( s_appon ) 
            except ImportError: 
                raise CommandError( "Annot vocabulary app %s does not exist." % s_appon )

            # add missing vocabulary app into vocabulary table
            if not ( s_appon in  ls_queryset):
                s_vocabulary = s_appon
                s_vocabulary = s_vocabulary.replace('appon','')
                obj_n = SysAdminVocabulary( app=s_appon, vocabulary=s_vocabulary )
                obj_n.save()

            # check process filter 
            if ( s_appon in ls_apponfilter ):
                # don't process this vocabulary 
                self.stdout.write( "WARNING: vocabulary json file will not be updated because it is listed in appsavocabulary/filter_vocabulary.txt: "+s_appon )
                
            else:
                # extract source from vocabulary app name
                s_vocabulary = s_appon.split('_')[0]
                s_vocabulary = s_vocabulary.replace( 'appon','' )
                s_source = s_appon.split('_')[1]

                # get already downloaded vocabulary source files
                self.stdout.write( "Check for latest downloead vocabulary release date and version" )
                s_apponpath = s_appon+"/fixtures/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_*_*original.json"
                ls_apponfixture = glob.glob( s_apponpath ) 
                # get latest downloaded  file version, source, and release date
                # start values 
                i_file_release = 0
                i_year = 1955
                i_month = 11
                i_day = 5
                s_version = None
                for s_apponfixture in ls_apponfixture: 
                    # inbetween value
                    s_date = (s_apponfixture.split('/')[-1]).split('_')[0]
                    i_date = int(s_date)
                    #s_vocabulary 
                    #s_source 
                    s_file_version = ((s_apponfixture.split('/')[-1]).split('.')[-2]).split('_')[3]
                    if ( i_file_release < i_date ): 
                        # overwrite final value 
                        i_file_release = i_date
                        i_year = int(s_date[0:4])
                        i_month = int(s_date[4:6])
                        i_day = int(s_date[6:8])
                        s_version = s_file_version
                # if no vocabulary source file found
                if  (s_version == None):
                    s_version = "nop"
                    s_sourcefile =  s_appon+"/fixtures/19551105_"+s_vocabulary+"_"+s_source+"_"+s_version+"_original.json" 
                    open(s_sourcefile, 'w').close()
                # final value
                self.stdout.write( "Found source files: "+str(s_source)+' | '+str(s_vocabulary)+' | '+str(s_version)+' | '+str(i_file_release)+' | '+str(ls_apponfixture))
            

                # check against latest online version, downlod if newer version is aviable
                # this is vocabulary source specific code 
                self.stdout.write( "Check for latest online released version." )
                s_pathmedia=MEDIA_ROOT+"quarantine/"

                # own source
                if ( s_source == 'own' ):
                    self.stdout.write( "Own source." )
                    s_ontology = m_appon.models.TERM_SOURCE_NAME
                    s_urlhome = m_appon.models.TERM_SOURCE_FILE

                # ensembl.org source gtf
                elif ( s_source == 'ensembl' ):
                    s_ontology = m_appon.models.TERM_SOURCE_NAME
                    s_urlhome = m_appon.models.TERM_SOURCE_FILE
                    # get online vocabulary version
                    s_organism = m_appon.models.ORGANISM  # kind of the query 
                    s_url = m_appon.models.REST_URL  # http://rest.ensembl.org/info/species?content-type=application/json  
                    d_latest = ensemblorg2get_latest( s_url=s_url, s_organism=s_organism, s_pathmedia=s_pathmedia ) 
                    s_online_release = d_latest["s_release"]
                    s_online_release = s_online_release.replace( '-', '' )
                    i_online_release = int( s_online_release )
                    s_online_version = d_latest["s_version"]
                    self.stdout.write( "Found source online release and version: "+s_online_version+' | '+s_online_release )
                    # check against lated downloaded file version 
                    if ( i_file_release < i_online_release ): 
                        # get model 
                        s_model = s_appon.split('_')[0]
                        s_model = s_model.replace('appon','')
                        s_model = s_appon+"."+s_model
                        # unzip source file 
                        self.stdout.write( "Unzip sourcefile." )
                        s_file = d_latest["s_file"]
                        os.system( "gunzip -kfv "+s_file )
                        s_ifile = re.sub(".gz$","", s_file)
                        # generate vocabulary json file 
                        self.stdout.write( "Generate json file." )
                        s_ofile = s_appon+"/fixtures/"+s_online_release+"_"+s_vocabulary+"_"+s_source+"_"+s_online_version+"_original.json"
                        gtf2jsonontology( s_ifile=s_ifile, s_ofile=s_ofile, s_model=s_model, s_release=d_latest["s_release"], s_version=s_online_version, b_swaptermid=m_appon.models.SWAP_TERMID, b_lowerpk=m_appon.models.LOWER_PK )
                        # update vocabulary status
                        self.stdout.write( "Update vocabulary status." )
                        i_year = int(s_online_release[0:4])
                        i_month = int(s_online_release[4:6])
                        i_day = int(s_online_release[6:8])
                        s_version = s_online_version
                    else : 
                        self.stdout.write( "No newer online releasd version detected." )

                # uniprot.org source http
                elif ( s_source == 'uniprot' ):
                    s_ontology = m_appon.models.TERM_SOURCE_NAME
                    s_urlhome = m_appon.models.TERM_SOURCE_FILE
                    # get online version
                    s_contact = crowbar.CONTACT
                    s_query = '(organism:9606) AND (keyword:Reference proteome)'  # latest homo sapiens reference proteom 
                    s_url = m_appon.models.REST_URL  # http://www.uniprot.org/uniprot/ 
                    o_httpresponse =  uniprotorg2get_httpresponse( s_url=s_url, s_query=s_query, s_contact=s_contact )
                    d_latest = uniprotorg2extract_header( o_httpresponse=o_httpresponse )
                    s_online_version = d_latest['X-UniProt-Release']
                    s_online_release = s_online_version
                    s_online_release = s_online_release.replace( '-', '' )
                    s_online_release = s_online_release+'01'
                    i_online_release = int( s_online_release )
                    self.stdout.write( "Found source online version: "+s_online_release+' | ' +s_online_version )
                    # check against lated downloaded file version 
                    if ( i_file_release < i_online_release ): 
                        # get model
                        s_model = s_appon.split('_')[0]
                        s_model = s_model.replace('appon','')
                        s_model = s_appon+"."+s_model
                        # download and extract latest ontolology term set
                        self.stdout.write( "Download and extract latest version." )
                        lds_page =  uniprotorg2extract_page( o_httpresponse=o_httpresponse, s_pathmedia=s_pathmedia )
                        ld_out = uniprotorg2pack_proteom( lds_page=lds_page, ds_header=d_latest, s_model=s_model, b_swaptermid=m_appon.models.SWAP_TERMID, b_lowerpk=m_appon.models.LOWER_PK )
                        # generate vocabulary json file
                        self.stdout.write( "Generate json file." )
                        s_ofile = s_appon+'/fixtures/'+s_online_release+'_'+s_vocabulary+'_'+s_source+'_'+s_online_version+'_original.json' 
                        f_file = open( s_ofile, 'w' ) # open file handle
                        json.dump( ld_out, f_file,  sort_keys=True, indent=4 )
                        # update vocabulary status
                        self.stdout.write( "Update ontology status." )
                        i_year = int(s_online_release[0:4])
                        i_month = int(s_online_release[4:6])
                        i_day = int(s_online_release[6:8])
                        s_version = s_online_version
                    else : 
                        self.stdout.write( "No newer online version detected." )

                # bioontology.org source
                elif ( s_source == 'bioontology' ):
                    s_ontology = m_appon.models.TERM_SOURCE_NAME
                    s_urlhome = m_appon.models.TERM_SOURCE_FILE
                    # get online vocabulary version
                    s_apikey = crowbar.APIKEY
                    s_url = m_appon.models.REST_URL+"/ontologies/"+m_appon.models.TERM_SOURCE_NAME+"/latest_submission"  # http://data.bioontology.org
                    d_latest = bioontologyorg2extract_version( s_url=s_url, s_apikey=s_apikey ) 
                    s_online_release = d_latest["date"]
                    s_online_release = s_online_release.replace( '-', '' )
                    i_online_release = int( s_online_release )
                    s_online_version = d_latest["version"]
                    self.stdout.write( "Found source online version: "+s_online_release+' | '+s_online_version )
                    # check against lated downloaded file version 
                    if ( i_file_release < i_online_release ):
                        self.stdout.write( "Handle latest released version." ) 
                        # get model
                        s_model = s_appon.split('_')[0]
                        s_model = s_model.replace('appon','')
                        s_model = s_appon+"."+s_model 
                        # http
                        if ( m_appon.models.FORMAT == "http" ):
                            # download and extract latest vocabulary term set
                            self.stdout.write( "Download and extract latest version." )
                            s_url = m_appon.models.REST_URL+"/ontologies/"+m_appon.models.TERM_SOURCE_NAME+"/classes/"+m_appon.models.CLASS_URL
                            d_branch =  bioontologyorg2extract_branch( s_url=s_url, s_apikey=s_apikey, b_getbranchcut=m_appon.models.GET_BRANCHCUT )
                            ld_out = bioontologyorg2pack_branch( d_branch=d_branch, d_version=d_latest, s_model=s_model,  b_swaptermid=m_appon.models.SWAP_TERMID, b_lowerpk=m_appon.models.LOWER_PK )
                            # generate vocabulary json file
                            self.stdout.write( "Generate json file." )
                            s_file = s_appon+'/fixtures/'+s_online_release+'_'+s_vocabulary+'_'+s_source+'_'+s_online_version+'_original.json' 
                            f_file = open( s_file, 'w' ) # open file handle
                            json.dump( ld_out, f_file,  sort_keys=True, indent=4 )
                        # obo
                        elif (  m_appon.models.FORMAT == "obo" ): 
                            # download latest source file 
                            self.stdout.write( "Download and extract latest version." )
                            s_url = m_appon.models.REST_URL+"/ontologies/"+m_appon.models.TERM_SOURCE_NAME+"/download"
                            s_file = s_pathmedia+m_appon.models.TERM_SOURCE_NAME+".obo"
                            bioontologyorg2get_file( s_url=s_url, s_apikey=s_apikey, s_ofile=s_file )
                            # generate vocabulary json file
                            self.stdout.write( "Generate json file." )
                            s_ofile = s_appon+"/fixtures/"+s_online_release+"_"+s_vocabulary+"_"+s_source+"_"+s_online_version+"_original.json"
                            obo2jsonontology( s_ifile=s_file, s_ofile=s_ofile, s_model=s_model, s_release=d_latest["date"], s_version=s_online_version )
                        # umls
                        elif (  m_appon.models.FORMAT == "umls" ):
                            # download latest source file 
                            self.stdout.write( "Download and extract latest version." )
                            s_url = m_appon.models.REST_URL+"/ontologies/"+m_appon.models.TERM_SOURCE_NAME+"/download"
                            s_file = s_pathmedia+m_appon.models.TERM_SOURCE_NAME+".umls"
                            bioontologyorg2get_file( s_url=s_url, s_apikey=s_apikey, s_ofile=s_file )
                            # generate vocabularyjson file 
                            self.stdout.write( "Generate json file." )
                            s_ofile = s_appon+"/fixtures/"+s_online_release+"_"+s_vocabulary+"_"+s_source+"_"+s_online_version+"_original.json"
                            umls2jsonontology( s_ifile=s_file, s_ofile=s_ofile, s_model=s_model, s_release=d_latest["date"], s_version=s_online_version )
                        # error
                        else :
                            sys.exit( "Error : unknown vocabulary source format "+str(m_appon.models.FORMAT)+" ." )
                        # update vocabulary status
                        self.stdout.write( "Update vocabulary status." )
                        i_year = int(s_online_release[0:4])
                        i_month = int(s_online_release[4:6])
                        i_day = int(s_online_release[6:8])
                        s_version = s_online_version
                    else : 
                        self.stdout.write( "No newer online version detected." )

                # unknown source
                else :  
                    self.stderr.write( "Error: unown source "+s_source+". Addapt appsavocabulary/management/commands/vocabulary_getupdate.py." )

                # update vocabulary status
                self.stdout.write("Update vocabulary status" )
                obj_n = SysAdminVocabulary.objects.get( app=s_appon )
                #obj_n.app 
                #obj_n.vocabulary
                obj_n.ontology = s_ontology
                obj_n.url = s_urlhome
                obj_n.version_latest = s_version # downloaed from the online source 
                obj_n.version_latest_date  = datetime.date( i_year,i_month,i_day ) # downloaed from the online source 
                #obj_n.version_loaded # uploaed into annot database 
                #obj_n.version_loaded_date  # uploaed into annot database  
                obj_n.up_to_date = obj_n.version_loaded_date >= obj_n.version_latest_date  # uploaed into annot database 
                obj_n.save()
