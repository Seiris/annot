# import form django 
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import python
import datetime 
import glob
import importlib
import inspect
import json

# import annot
from appsavocabulary.models import SysAdminVocabulary
#from prjannot.settings import BASE_DIR

class Command( BaseCommand ):
    args = "<apponmmm_source apponmmm_source ...>"
    help = "Reload controled vocabulary backup."

    def handle(self, *args, **options): 
        # initiate 
        queryset = SysAdminVocabulary.objects.all()  # get queryset
        ls_queryset = []

        # treat argument specified vocabulary
        if ( len(args) > 0 ): 
            # get vocabularies app 
            ls_appon = list( args )
            for obj_n in queryset:
                # check if vocabulary app in vocabulary table exist  
                if ( str(obj_n.app) in ls_appon):
                    ls_queryset.append( str(obj_n.app) )  # populate queryset list 
        # treat all vocabularies
        else:
            # get vocabularies app 
            ls_appon = glob.glob('appon*') 
            ls_appon.sort()
            # reset queryset 
            self.stdout.write("Reset vocabulary table" )
            for obj_n in queryset: 
                # check if vocabulary app in vocabulary table exist  
                if ( str(obj_n.app) in ls_appon):
                    ls_queryset.append( str(obj_n.app) )  # populate queryset list 
                else:
                    obj_n.delete()  # delete vocabulary out of table 

        # process vocabulary app
        for s_appon in ls_appon:

            # get latest backup json file version 
            self.stdout.write( "\nProcessing backup restore: "+s_appon )
            self.stdout.write( "Check for latest vocabulary backup" )
            s_apponpath = s_appon+"/fixtures/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_*_*backup.json"
            ls_apponfixture = glob.glob( s_apponpath ) 
            # start values 
            i_file_release = 0
            s_version = None
            for s_apponfixture in ls_apponfixture: 
                # inbetween value
                s_date = (s_apponfixture.split('/')[-1]).split('_')[0]
                i_date = int(s_date)
                #s_vocabulary 
                #s_source 
                s_file_version = ((s_apponfixture.split('/')[-1]).split('.')[-2]).split('_')[3]
                if ( i_file_release < i_date ): 
                    # overwrite final value 
                    i_file_release = i_date
                    s_version = s_file_version
                    s_filebackuplatest = s_apponfixture 

            if  (s_version != None):
                # if backupfile found
                self.stdout.write( "Found backup files: "+s_filebackuplatest+' | '+str(ls_apponfixture) )

                # load latest database version  
                self.stdout.write( "\nLoad latest database version: "+s_appon )
                try:
                    m_appon = importlib.import_module( s_appon+".models" ) 
                    for s_name, obj in inspect.getmembers( m_appon ):
                        if inspect.isclass( obj ):
                            c_apponmodels = obj  # eqivalent to json model
                except ImportError: 
                    raise CommandError( "Annot vocabulary app %s does not exist." % s_appon )

                # load latest backup json file version
                self.stdout.write( "Load latest backup json file version into the database.")
                f_fixture = open( s_filebackuplatest, 'r' )  # open file handle
                ld_json = json.load( f_fixture )

                # loop through backup json file version 
                for d_json in ld_json :
                    s_term_name = d_json ["fields"]["term_name"]
                    try: 
                        # check if json file entry term allredy in database
                        c_apponmodels.objects.get( term_name=s_term_name)
                    except c_apponmodels.DoesNotExist:
                        # write missing json file entry into database 
                        s_date = d_json["fields"]["term_source_version_update"]
                        ls_date = s_date.split('-')
                        i_year = int(ls_date[0])
                        i_month = int(ls_date[1])
                        i_day = int(ls_date[2])
                        o_newrecord = c_apponmodels( term_name=s_term_name, term_id= d_json["fields"]["term_id"], annot_id=d_json["pk"], term_source_version_responsible="term_from_original_source", term_source_version_update=datetime.date( i_year,i_month,i_day ), term_source_version= d_json["fields"]["term_source_version"], term_ok=True )
                        o_newrecord.save()

                # python manage.py vocabulary_backup s_appon
                management.call_command( "vocabulary_loadupdate", s_appon, )

            else :
                # if no backupfile found 
                self.stdout.write( "No backup file found." )
