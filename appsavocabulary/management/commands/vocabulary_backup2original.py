# import from django 
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import from python 
import glob
import re
import sys
import json

# import annot
from appsavocabulary.models import SysAdminVocabulary


class Command( BaseCommand ): 
    args = "<apponmmm_source apponmmm_source ...>"
    help = "Transform latest controlled vocabulary backup into an original vocabulary verion. (This code will work only for own controled vocabulary.)"
    def handle( self, *args, **options ):
        # initiate 
        b_flag = False
        queryset = SysAdminVocabulary.objects.all()  # get queryset
        ls_queryset = []

        # treat argument specified vocabulary
        if ( len( args ) > 0 ): 
            # get vocabularies app 
            ls_appon = list( args )
            for obj_n in queryset:
                # check if vocabulary app in vocabulary table exist  
                if ( str( obj_n.app ) in ls_appon):
                    ls_queryset.append( str( obj_n.app ) )  # populate queryset list 
        # treat all vocabularies
        else:
            # get vocabularies app 
            ls_appon = glob.glob( "appon*" ) 
            ls_appon.sort()
            # reset queryset 
            self.stdout.write( "Reset sys admin vocabulary table" )
            for obj_n in queryset: 
                # check if vocabulary app in vocabulary table exist  
                if ( str( obj_n.app ) in ls_appon):
                    ls_queryset.append( str( obj_n.app ) )  # populate queryset list 
                else:
                    obj_n.delete()  # delete vocabulary out of table 

        # process vocabulary app
        for s_appon in ls_appon:
            self.stdout.write( "\nProcess vocabulary: "+s_appon+" ..." )

            # own vocabulary 
            if ( re.search( r"_own$", s_appon ) != None ):

                # find latest json file backup version
                self.stdout.write( "Search latest json file version" )
                obj_n = SysAdminVocabulary.objects.get( app=s_appon )
                s_vocabulary = obj_n.vocabulary
                #s_version_latest =  obj_n.version_latest
                s_regexfixture = s_appon+"/fixtures/*_"+s_vocabulary+"_*_backup.json"
                ls_backuppathfile = glob.glob( s_regexfixture )
                i_latest = 0
                s_backuplatest = None
                for s_backuppathfile in ls_backuppathfile:
                    s_backupfile = s_backuppathfile.split('/')[-1]
                    s_date = s_backupfile.split('_')[0]
                    i_date = int( s_date )
                    if ( i_latest < i_date ):
                        s_backuplatest = s_backuppathfile
                        i_latest = i_date
                self.stdout.write( "Found "+str( s_backuplatest ) ) 
 
                # if backup found 
                if not ( s_backuplatest == None ):
                    self.stdout.write( "Generate original release file" )        

                    # generate original release path file name
                    s_ifile = s_backuplatest.split('/')[-1]
                    s_iversion =s_ifile.split('_')[-2] 
                    s_oversion = s_ifile.split('_')[0]
                    s_odate = s_oversion[0:4]+'-'+s_oversion[4:6]+'-'+s_oversion[6:8]
                    s_ofile = s_ifile.replace( s_iversion+"_backup", s_oversion+"_original") 
                    s_opathfile = s_backuplatest.replace( s_ifile, s_ofile )

                    # read input
                    with open( s_backuplatest, 'r') as f_in:
                        ld_in = json.load( f_in )
                    f_in.closed

                    # generate output
                    ld_out = []
                    for d_in in ld_in :
                        d_out = {}
                        d_field = {}
                        d_field["term_name"] = d_in["fields"]["term_name"]
                        d_field["term_id"] = d_in["pk"]
                        d_field["term_source_version_responsible"] = "term_from_original_source"
                        d_field["term_source_version_update"] = s_odate
                        d_field["term_source_version"] = s_oversion 
                        d_field["term_ok"] = True 
                        d_out["fields"] = d_field
                        d_out["model"] = d_in["model"]
                        d_out["pk"] = d_in["pk"]
                        ld_out.append( d_out )

                    # write output
                    with open( s_opathfile, 'w' ) as f_out: 
                        json.dump( ld_out, f_out, sort_keys=True, indent=4 )
                    f_out.close()
                    self.stdout.write( "Written "+s_opathfile )        

                # if no backup found 
                else : 
                    self.stdout.write( "No operation. No backupfile found." ) 

            # not own vocabulary
            else : 
                self.stdout.write( "No operation. Only a own vocabulary can be transformd from a backup version to an original release." )
