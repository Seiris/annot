# import form djnago 
from django.conf.urls import patterns, url
# import from appsaontology
from appsavocabulary import views


urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^export/$', views.export, name="export"),
)
