# import for django
from django.shortcuts import render
from django.http import HttpResponse
from django.core import management

# import from python 
import datetime
import glob
import json
import os
import zipfile
# import from annot 
from prjannot.settings import BASE_DIR
from prjannot.settings import MEDIA_ROOT


# Create your views here.
# index 
def index( request ):
    return HttpResponse("Hello, world. You're at the appsavocabulary index.")


# export  
def export( request ): # sappon
    os.chdir( BASE_DIR )
    s_versiontype = request.GET.get('versiontype')
    s_choice = request.GET.get('choice')

    # opem zip file handle 
    s_date = str(datetime.date.today())
    s_date = s_date.replace('-','')
    if ( s_versiontype == 'original'):
        s_filezip = s_date+"_annot_vocabulary_original_version_download.zip"
    else: 
        s_filezip = s_date+"_annot_vocabulary_backup_version_download.zip"
    s_pathfilezip = MEDIA_ROOT+"quarantine/"+s_filezip  #attachment 
    with zipfile.ZipFile(s_pathfilezip, 'w') as onzip:

        # for each vocabulary
        ls_choice = s_choice.split(',')
        for s_appon in ls_choice: 
            s_vocabularysource = s_appon.replace( "appon",'' )

            # distinquish between original and backupversion case
            if ( s_versiontype == 'original'):
                # get latest online version by:  python manage.py vocabulary_downloadoriginal  s_appon
                management.call_command( "vocabulary_loadupdate", s_appon, )
                # get vocabulary source files
                s_apponpath = s_appon+"/fixtures/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_*_*_orginal.json"
            else: 
                # get latest online version by:  python manage.py vocabulary_downloadoriginal  s_appon
                management.call_command( "vocabulary_backup", s_appon, )
                # get vocabulary source files
                s_apponpath = s_appon+"/fixtures/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_*_*_backup.json"
            ls_apponfixture = glob.glob( s_apponpath ) 

            # get latest downloaded  file version, source, and release date
            # start values 
            i_file_release = 0
            i_year = 1955
            i_month = 11
            i_day = 5
            s_version = None
            for s_apponfixture in ls_apponfixture: 
                # inbetween value
                s_date = (s_apponfixture.split('/')[-1]).split('_')[0]
                i_date = int(s_date)
                #s_vocabulary 
                #s_source 
                s_file_version = ((s_apponfixture.split('/')[-1]).split('.')[-2]).split('_')[3]
                if ( i_file_release < i_date ): 
                    # overwrite final value 
                    i_file_release = i_date
                    i_year = int(s_date[0:4])
                    i_month = int(s_date[4:6])
                    i_day = int(s_date[6:8])
                    s_version = s_file_version
            # final value, distinquish between original and backupversion case
            if ( s_versiontype == 'original'): 
                s_filejson = str(i_file_release)+'_'+s_vocabularysource+'_'+str(s_version)+"_original.json"
            else:
                 s_filejson = str(i_file_release)+'_'+s_vocabularysource+'_'+str(s_version)+"_backup.json"
            s_pathfilejson = s_appon+"/fixtures/"+s_filejson

            # pack latest version into zip
            onzip.write( s_pathfilejson, arcname=s_filejson )

    # push larest version save as zip
    response = HttpResponse( content_type="apploication/zip" )   # apploication/json
    response["Content-Disposition"] = "attachment; filename="+s_filezip
    f = open( s_pathfilezip, "rb")
    response.write( f.read() ) 

    # delete zip file 
    #os.system( "rm "+s_pathfilezip )

    # return 
    return( response )
    #return HttpResponse("Hello, world. You're at the appsavocabulary exportjson page."+s_filejson)
