# import form django
from django.contrib import admin
from django.core import management
from django.contrib.contenttypes.models import ContentType
#from django.http import HttpResponse
from django.http import HttpResponseRedirect
#from django.core import serializers
#from prjannot.settings import BASE_DIR
# iporyt from python 
#import json
#import os
# import from prj
from appsavocabulary.models import SysAdminVocabulary 


# Register your models here.
#admin.site.register( SysAdminVocabulary )
class SysAdminVocabularyAdmin( admin.ModelAdmin ):
    # view
    list_display = ("app","vocabulary","ontology","url","version_latest","version_latest_date","version_loaded","version_loaded_date","original_term_count","up_to_date") 
    readonly_fields = ("app","vocabulary","ontology","url","version_latest","version_latest_date","version_loaded","version_loaded_date","original_term_count","up_to_date") 
    search_fields = ("app","vocabulary","ontology","url") 
    actions = ["vocabulary_getupdate","vocabulary_loadupdate","download_original_version", "vocabulary_backup","vocabulary_loadbackup", "download_backup_version"] 

    # action checking_for_original_update
    def vocabulary_getupdate( self, request, queryset ):  # self = modeladmin
        for obj_n in queryset:
            # python manage.py vocabulary_getupdate s_appon
            s_appon = str( obj_n.app )
            management.call_command( "vocabulary_getupdate", s_appon, )
            self.message_user( request, s_appon+" successfully checked for available updates." )
    vocabulary_getupdate.short_description = "Check for original vocabulary updates"

    # action load_latest_original
    def vocabulary_loadupdate( self, request, queryset ):  # self = modeladmin
        for obj_n in queryset:
            # python manage.py vocabulary_loadupdate s_appon   
            s_appon = str( obj_n.app )
            management.call_command( "vocabulary_loadupdate", s_appon, )
            self.message_user( request, s_appon+" successfully updated." ) 
    vocabulary_loadupdate.short_description = "Update selected vocabularies"

    # action download original vocabulary version  
    def download_original_version( self,  request, queryset ): 
        # list queryset obj names
        ls_choice = request.POST.getlist( admin.ACTION_CHECKBOX_NAME )
        # call appsavocabulary view export function  
        return( HttpResponseRedirect( "/appsavocabulary/export?versiontype=orignal&choice=%s" % (",".join( ls_choice )) ) )
    download_original_version.short_description = "Downolad selected vocabularies original version"


    # action backup_vocabulary
    def vocabulary_backup( self, request, queryset ):  # self = modeladmin
        for obj_n in queryset: 
            # python manage.py vocabulary_backup s_appon
            s_appon = str( obj_n.app )
            management.call_command( "vocabulary_backup", s_appon, )
            self.message_user( request, s_appon+" successfully backuped." ) 
    vocabulary_backup.short_description = "Backup selected vocabularies" 

    # action restore_backup
    def vocabulary_loadbackup( self, request, queryset ):  # self = modeladmin
        for obj_n in queryset:
            # python manage.py vocabulary_loadupdate s_appon   
            s_appon = str( obj_n.app )
            management.call_command( "vocabulary_loadbackup", s_appon, )
            self.message_user( request, s_appon+" successfully restored." ) 
    vocabulary_loadbackup.short_description = "Restore latest backup for selected vocabularyies"

    # action downlaod backup version 
    def download_backup_version(  self,  request, queryset ):
        # list queryset obj names
        ls_choice = request.POST.getlist( admin.ACTION_CHECKBOX_NAME )
        # call appsavocabulary view export function  
        return( HttpResponseRedirect( "/appsavocabulary/export?versiontype=backup&choice=%s" % (",".join( ls_choice )) ) )
    download_backup_version.short_description = "Downolad selected vocabularies backup version" 

    # action transform backup version to original release
    # bue 20150220: to be save from beeing messed up, this command will only be available from the command line by python manage.py vocabulary_backup2original 

    # register
admin.site.register( SysAdminVocabulary, SysAdminVocabularyAdmin ) 
