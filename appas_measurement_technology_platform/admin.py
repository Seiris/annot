from django.contrib import admin
from appas_measurement_technology_platform.models import AssayMeasureTechPlatform, StudyAssayMeasureTechPlatform

# Register your models here.
#admin.site.register( AssayMeasureTechPlatform )
class AssayMeasureTechPlatformAdmin( admin.ModelAdmin ):
    fieldsets = [
        ( None, { 'fields' : [ 'a_name' ] } ), 
        ( 'Sample', { 'fields' : [ 'a_sample_name', 'a_extract_name', 'a_label_extract_name' ] } ), 
        ( 'Lab protocol', { 'fields' : [ 'a_parameter', 'a_parameterset', 'a_factor','a_factorset','a_protocol','a_imagefile','a_rawdatafile','a_deriveddatafile','a_imagefile_url','a_rawdatafile_url','a_deriveddatafile_url' ] } ), 
    ] 
    list_display = ('a_name','a_sample_name')
    search_fields = ('a_name','a_sample_name')
admin.site.register( AssayMeasureTechPlatform, AssayMeasureTechPlatformAdmin )


#admin.site.register( StudyAssayMeasureTechPlatform )
class StudyAssayMeasureTechPlatformAdmin( admin.ModelAdmin ):
    list_display = ('study','assay')
    search_fields = ('study','assay')
admin.site.register( StudyAssayMeasureTechPlatform, StudyAssayMeasureTechPlatformAdmin )
