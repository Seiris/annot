from django.contrib import admin
from apponsex_bioontology.models import Sex


# Register your models here.
#admin.site.register( Sex )
class SexAdmin( admin.ModelAdmin ):
    list_display = ('term_name','annot_id','term_source_version_responsible','term_source_version_update','term_source_version','term_id','term_ok') 
    readonly_fields = ('term_source_version_update','term_source_version','term_id','term_ok',) 
    search_fields = ('term_name','term_id','annot_id') 

# register
admin.site.register( Sex, SexAdmin ) 
