from django.db import models
# import ontologies
# import bridges 

# Create your models here.
class Person( models.Model ): 
    #objects = PersonManager()
    person_identifier =  models.SlugField( max_length=32, primary_key=True, default="not_yet_specified", help_text="Uniq person identifier." )
    person_first_name =  models.SlugField( max_length=32, help_text="The first name of a person." )
    person_mid_initials =  models.SlugField( max_length=32, help_text="The middle initials of a person.", blank=True )
    person_last_name =  models.SlugField( max_length=32, help_text="The last name of a person." )
    person_email = models.EmailField( default="not@yet.specified", help_text="The email address of a person." )
    person_phone = models.CharField( default="not_yet_specified", max_length=32, help_text="The telephone number of a person." )
    person_fax =  models.CharField( default="not_yet_specified", max_length=32, help_text="The fax number of a person." )
    person_address =  models.CharField( default="not_yet_specified", max_length=255, help_text="The address of a person." )
    person_affiliation = models.CharField( default="not_yet_specified", max_length=255, help_text="The organization affiliation." )
    def __str__( self ):
        return( self.person_identifier )

    class Meta:
        ordering = ["person_identifier"]
