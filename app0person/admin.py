from django.contrib import admin
from app0person.models import Person

# Register your models here.
#admin.site.register( Person )
class PersonAdmin( admin.ModelAdmin ): 
    list_display = ('person_identifier','person_first_name','person_mid_initials','person_last_name','person_email','person_phone','person_fax','person_address','person_affiliation')
    search_fields = ('person_identifier','person_last_name','person_first_name','person_mid_initials')
admin.site.register( Person, PersonAdmin )
