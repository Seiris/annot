from django.contrib import admin
from django.core import management
from appsabrick.models import Reagent, SysAdminReagent, SysAdminBrick

# Register your models here.
#admin.site.register( Reagent )
class ReagentAdmin( admin.ModelAdmin ):
    list_display = ("reagent_type","reagent_name","up_to_date")
    readonly_fields = ("reagent_type","reagent_name","up_to_date")
    search_fields = ("reagent_type","reagent_name")
admin.site.register( Reagent, ReagentAdmin )


#admin.site.register( SysAdminReagent )
class SysAdminReagentAdmin( admin.ModelAdmin ):
    # view
    list_display = ("app","reagent_model","reagent_type","reagent_aliquot_count","reagent_last_vocabulary_pull")  #deltatime_day
    readonly_fields = ("app","reagent_model","reagent_type","reagent_aliquot_count","reagent_last_vocabulary_pull")  #deltatime_day
    search_fields = ("app","reagent_model","reagent_type")
    actions = ["reagent_pull"]

    # action pull latest reagent vocabularies 
    def reagent_pull( self, request, queryset ):  # self = modeladmin
        # python manage.py reagent_pull s_reagenttype
        for obj_n in queryset:
            s_reagenttype = str( obj_n.reagent_type )
            management.call_command( "reagent_pull", s_reagenttype )
            self.message_user( request, "Successfully pulled in available updates for "+s_reagenttype )
    reagent_pull.short_description = "Pull latest vocabulary for selected reagent types"

# register
admin.site.register( SysAdminReagent, SysAdminReagentAdmin )


#admin.site.register( SysAdminBrick ) 
class SysAdminBrickAdmin( admin.ModelAdmin ):
    # view
    list_display = ("app","model","model_ancestor","backup_hierarchy_level","latest_backup") 
    readonly_fields = ("app","model","model_ancestor","backup_hierarchy_level","latest_backup")
    search_fields = ("app","reagent_model","reagent_type") 
    actions = ["annot_backup","annot_loadbackup"]

    # action backup annot model content 
    def annot_backup( self, request, queryset ):  # self = modeladmin 
        # python manage.py annot_backup s_model
        for obj_n in queryset:
            s_model = str( obj_n.model )
            management.call_command( "annot_backup", s_model )
            self.message_user( request, s_model+" successfully backuped." )
    annot_backup.short_description = "Backup selected annot model content."

    # reload latest annot model content
    def annot_loadbackup( self, request, queryset ): # self = modeladmin
        # python manage.py annot_loadbackup s_model
        for obj_n in queryset:
            s_model = str( obj_n.model )
            management.call_command( "annot_loadbackup", s_model )
            self.message_user( request, s_model+" backup successfully reloaded." )
    annot_loadbackup.short_description = "Reloead latest aviable backup for selected annot models."

# register
admin.site.register( SysAdminBrick, SysAdminBrickAdmin )
