# import from django
from django.core.management.base import BaseCommand, CommandError
from django.db.models import get_model
# import from python
import datetime
import importlib
import os
# import from annot
from prjannot.settings import BASE_DIR
from appsabrick.models import REAGENTRECORD, Reagent, SysAdminReagent

# constants app model type


### main ### 
class Command( BaseCommand ):
    args = "<apponmmm_source apponmmm_source ...>"
    help = "Pull latest reagent vocabularies."

    def handle( self, *args, **options ):
        # initiate
        os.chdir( BASE_DIR )
        queryset = SysAdminReagent.objects.all()  # set query set

        # populate reagnet type dictionary of dictionary from REAGENTRECORDS
        self.stdout.write( "\nPopulate reagent type dictionary." )
        dds_reagenttype = {}
        for ls_record in REAGENTRECORD: 
            ds_reagenttype = {}
            ds_reagenttype.update( {"app": ls_record[0]} ) 
            ds_reagenttype.update( {"reagent_model":ls_record[1]} )
            ds_reagenttype.update( {"reagent_type":ls_record[2]} ) 
            dds_reagenttype.update( {ls_record[2]:ds_reagenttype} )
        # populate list of reagent types 
        ls_reagenttype = list( dds_reagenttype.keys() )

        # populater sys admin reagent type tabe with missing types 
        self.stdout.write( "Update reagen type tabe." )
        for s_reagenttype in ls_reagenttype :
            try: 
                obj_sysadminreagent = SysAdminReagent.objects.get( reagent_type=s_reagenttype ) 
            except SysAdminReagent.DoesNotExist: 
                obj_sysadminreagent = SysAdminReagent( app=dds_reagenttype[s_reagenttype]["app"], reagent_model=dds_reagenttype[s_reagenttype]["reagent_model"], reagent_type=dds_reagenttype[s_reagenttype]["reagent_type"], reagent_last_vocabulary_pull=datetime.datetime( year=1955, month=11, day=5 ) )
                obj_sysadminreagent.save() 
        # update ls_reagenttype and queryset 
        queryset = SysAdminReagent.objects.all() 
        ls_reagenttype = []
        for obj_sysadminreagent in queryset: 
            s_reagenttype = str( obj_sysadminreagent.reagent_type )
            ls_reagenttype.append( s_reagenttype )  

        # if argument specified reagent types
        if ( len(args) > 0 ): 
            self.stdout.write( "Check if args reagent type are valid." )
            ls_reagenttype = []  # reset list of reagent types   
            ls_reagenttype_args = list( args ) # get reagent type arguments
            # for each agument specified reagent type 
            for obj_sysadminreagent in queryset:
                s_reagenttype = str( obj_sysadminreagent.reagent_type )
                # check if reagent type argument valid 
                if ( s_reagenttype in ls_reagenttype_args ):
                    # populate list of valid reagent types
                    ls_reagenttype.append( s_reagenttype )  
                    self.stdout.write( "reagent type found in args: "+s_reagenttype )
                else:
                    # give a warning
                    self.stdout.write( "reagent type not found in args: "+s_reagenttype )


        ### process regenttype ###
        for s_reagenttype in ls_reagenttype:
            self.stdout.write( "\nProcessing reagent type: "+s_reagenttype )
            # get sys admin reagent table content for this reagent type
            obj_sysadminreagent = SysAdminReagent.objects.get( reagent_type=s_reagenttype ) 
            s_app = obj_sysadminreagent.app
            s_reagentmodel = obj_sysadminreagent.reagent_model

            # reset reagent stack
            self.stdout.write( "Reset this reagent types reagent stack terms." )
            try :
                obj_reagnetstack = Reagent.objects.filter( reagent_type=s_reagenttype )
                for obj_reagnet in obj_reagnetstack: 
                    # reset reagent 
                    obj_reagnet.up_to_date = False
                    obj_reagnet.save()
            except Reagent.DoesNotExist: 
                pass

            # pull the app table.
            self.stdout.write( "Pull latest reagent type related reagent terms." )
            obj_reagentmodel = get_model( s_app, s_reagentmodel)
            obj_reagentpull = obj_reagentmodel.objects.all() 
            i_aliquotcount = len(obj_reagentpull)
            print( "BUE :", obj_reagentmodel, obj_reagentpull, i_aliquotcount )
            for obj_reagent in obj_reagentpull:
                # push reagent into reagent table
                s_reagentname = str(obj_reagent)
                print( "Handling :", s_reagentname )
                try: 
                    obj_n = Reagent.objects.get( reagent_name=s_reagentname, reagent_type=s_reagenttype )
                    obj_n.up_to_date = True
                except Reagent.DoesNotExist:
                    obj_n = Reagent( reagent_name=s_reagentname, reagent_type=s_reagenttype, up_to_date=True ) 
                obj_n.save()

            # put sys admin reagent content
            obj_sysadminreagent = SysAdminReagent( app=s_app, reagent_model=s_reagentmodel, reagent_type=s_reagenttype, reagent_aliquot_count=i_aliquotcount, reagent_last_vocabulary_pull=datetime.datetime.now() )
            obj_sysadminreagent.save()
