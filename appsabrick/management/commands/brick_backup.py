# import from django 
from django.core import management
from django.core.management.base import BaseCommand, CommandError
from django.db.models import get_models 

# import from python
#import importlib
import datetime
import os

# import from annot
from prjannot.settings import BASE_DIR
from appsabrick.models import ANNOTMODELRECORD, SysAdminBrick


### main ### 
class Command( BaseCommand ):
    args = "<apponmmm_source apponmmm_source ...>"
    help = "Backup  annot model's content."

    def handle( self, *args, **options ):
        # initiate
        os.chdir( BASE_DIR )
            
        # get all annot models. this does not inclide appvo* controlled vocabulary models and appsa* system administration models!
        self.stdout.write( "Get all annot model listing out of each annot app0* and annot appas_* app." )
        ls_annotmodel_real = []  # reset ls_annotmodel_real
        lo_model = get_models()  # get all models from all annot apps
        # filter out sys admin annot relevant models 
        for o_model in lo_model: 
            s_model = str(o_model)
            if ( "app0" in s_model ) or ( "appas_" in s_model ): 
                if not ( "Manager" in s_model ):
                    ls_fractal = s_model.split("'")
                    ls_fractal = ls_fractal[-2].split('.')
                    s_fractal = ls_fractal[-1]
                    # populate ls_annotmodel_real
                    ls_annotmodel_real.append( s_fractal )
        self.stdout.write( "found : "+str(ls_annotmodel_real) )


        # get hierarchical ordered annot model list from ANNOTMODELRECORD
        self.stdout.write( "Get hierarchical ordered annot model listing from ANNOTMODELRECORD." )
        # populate annot model dictionary of dictionary
        d3ls_hierarchylevel_app_model_ancestor = {}
        for l_annotmodel  in ANNOTMODELRECORD:
            print( l_annotmodel )
            s_app = l_annotmodel[0]
            s_model = l_annotmodel[1]
            ts_ancestor = l_annotmodel[2]
            i_hierarchy = l_annotmodel[3]
            try: 
                catch = d3ls_hierarchylevel_app_model_ancestor[i_hierarchy][s_app][s_model]
                raise CommandError( "Error : entry alredy exist for: " %s_app  %s_model %i_hierarchy )
            except KeyError:
                # model dictionary
                try: 
                    d_model = d3ls_hierarchylevel_app_model_ancestor[i_hierarchy][s_app] #[s_model]
                except KeyError: 
                    d_model = {}
                # update 
                d_model.update({ s_model : ts_ancestor })
                # app dictionary
                try: 
                    d_app = d3ls_hierarchylevel_app_model_ancestor[i_hierarchy] # [s_app]
                except KeyError: 
                    d_app = {}
                # update
                d_app.update({ s_app : d_model })
                # hierarchy level dictionary 
                d3ls_hierarchylevel_app_model_ancestor.update({ i_hierarchy : d_app })


        # populate annot model list and annot model dictionary of tuple 
        ls_annotmodel = []
        dt_annotmodel = {}
        li_keyhierarchy = list( d3ls_hierarchylevel_app_model_ancestor.keys() )
        li_keyhierarchy.sort()
        # hierarchy level
        for i_keyheirarchie in li_keyhierarchy:
            ls_keyapp = list( d3ls_hierarchylevel_app_model_ancestor[i_keyheirarchie].keys() )
            ls_keyapp.sort()
            # app
            for s_keyapp in ls_keyapp: 
                ls_keymodel = list( d3ls_hierarchylevel_app_model_ancestor[i_keyheirarchie][s_keyapp].keys() )
                ls_keymodel.sort()
                # model
                for s_keymodel in ls_keymodel:
                    # ancestor 
                    ts_ancestor = d3ls_hierarchylevel_app_model_ancestor[i_keyheirarchie][s_keyapp][s_keymodel] 
                    t_record = ( s_keyapp, ts_ancestor, i_keyheirarchie )
                    # populate list of string and dcitionary of tuple
                    ls_annotmodel.append( s_keymodel )  # populate annot model list
                    dt_annotmodel.update({ s_keymodel : t_record })  # populate annot model dictionary of tuple


        # check if ls_annotmodel have missing models. models that can be found in ls_annotmodel_real. 
        self.stdout.write( "Check for missing models in ANNOTMODELRECORD." )
        for s_annotmodel in ls_annotmodel_real:
            if not(s_annotmodel in ls_annotmodel):
                raise CommandError( "annot model "+s_annotmodel+" found in  app0* or appas_* apps which is not defiend in the appsabrick models ANNOTMODELRECORD tuple. Specify the missing model in ANNOTMODELRECORD." )
            else:
                self.stdout.write( "ok "+s_annotmodel )


        # populate sys admin annot tabel with annot models 
        self.stdout.write( "Populate sys annot admin table with annot models." )
        for s_annotmodel in ls_annotmodel:
            # prepare input
            s_app = dt_annotmodel[s_annotmodel][0]
            s_ancestor = ""
            ts_ancestor = dt_annotmodel[s_annotmodel][1]
            print( "BUE :",s_annotmodel, ts_ancestor )            
            for s in ts_ancestor:
                print("BUE :", s)
                if ( len(s_ancestor) == 0 ):
                    s_ancestor = s
                else: 
                    s_ancestor = s_ancestor+'_'+s
            print("BUE :", s_ancestor+"\n")
            i_hierarchylevel = dt_annotmodel[s_annotmodel][2]
            # populate database table
            try: 
                obj_sysadminbrick = SysAdminBrick.objects.get( model=s_annotmodel )
                obj_sysadminbrick.app = s_app
                obj_sysadminbrick.model_ancestor = s_ancestor
                obj_sysadminbrick.backup_hierarchy_level = i_hierarchylevel
            except SysAdminAnnot.DoesNotExist: 
                obj_sysadminbrick = SysAdminBrick( app=s_app, model=s_annotmodel, model_ancestor=s_ancestor, backup_hierarchy_level=i_hierarchylevel, latest_backup=datetime.datetime(year=1955, month=11, day=5) )
            obj_sysadminbrick.save() 

        # delete models in sys admin annot table that are not in annomodel record
        lo_sysadminbrick = SysAdminBrick.objects.all()
        for o_sysadminbrick  in lo_sysadminbrick: 
            s_model = o_sysadminbrick.model
            if not( s_model in ls_annotmodel ):
                o_sysadminbrick.delete()


        # if argument specified annot models 
        if ( len(args) > 0 ):
            self.stdout.write( "Check if args annot models are valid.")
            ls_annotmodel_run = []  
            ls_annotmodel_args = list( args ) # get anot model arguments
            # for each agument specified annot model
            for s_annotmodel in ls_annotmodel_args:
                # check if reagent type argument valid 
                if ( s_annotmodel in ls_annotmodel ):
                    # populate list of valid reagent types 
                    ls_annotmodel_run.append( s_annotmodel )  
                    self.stdout.write( "ok "+s_annotmodel )
                else:
                    # give a warning
                    raise CommandError( "args specified annot model does not exist. "+s_annotmodel )
            # reset ls_annotmodel
            ls_annotmodel = ls_annotmodel_run


        # backup vocabulary
        #management.call_command( "vocabulary_backup" )

        # backup brick 
        # bue 20150310: back up is done on all lower levels models. the ancestor input is only for huamn beeings
        ls_backupedmodel = []
        for s_annotmodel in ls_annotmodel:
            self.stdout.write( "\nProcessing annot model: "+s_annotmodel )
            s_app = dt_annotmodel[s_annotmodel][0]
            #ts_ancestor = dt_annotmodel[s_annotmodel][1]
            i_hierarchy  =  dt_annotmodel[s_annotmodel][2]             
            # step through all hiersarchies 
            for i_keyheirarchy in li_keyhierarchy:  
                # check if running hierarchy is lower then s_annotmodel hierarchy
                if ( i_keyheirarchy <= i_hierarchy ): 
                    # loop through all lower level models
                    ls_keyapp = list( d3ls_hierarchylevel_app_model_ancestor[i_keyheirarchy] )
                    ls_keyapp.sort() 
                    for s_keyapp in ls_keyapp:
                        ls_keymodel = list( d3ls_hierarchylevel_app_model_ancestor[i_keyheirarchy][s_keyapp] )
                        for s_keymodel in ls_keymodel:
                            if not (s_keymodel in ls_backupedmodel):
                                # backup
                                self.stdout.write( "Backup annot model: "+s_keymodel )
                                s_model2backup = s_keyapp+'.'+s_keymodel
                                s_date = str(datetime.date.today())
                                s_date = s_date.replace('-','')
                                s_file = s_date+'_'+s_keymodel+'_'+s_keyapp+"_backup.xml"
                                s_pathfile = s_keyapp+"/fixtures/"+s_file
                                print("Bue :", s_pathfile, s_file, s_date, s_model2backup)
                                with open( s_pathfile, 'w') as f:
                                    # bue 20150329: desperate ResultProtocolRef make still troubles with --natural-foreign 
                                    #management.call_command( "dumpdata", s_model2backup, format="xml", indent=4, use_natural_foreign_keys=True, use_natural_primary_keys=True, stdout=f )
                                    management.call_command( "dumpdata", s_model2backup, format="xml", indent=4, use_natural_primary_keys=True, stdout=f )
                                # set file permission
                                os.chmod( s_pathfile, 0o775 )
                                # handle sys admin model latest_backup entry 
                                obj_sysadminbrick = SysAdminBrick.objects.get( model=s_keymodel )
                                obj_sysadminbrick.latest_backup = datetime.datetime.now()
                                obj_sysadminbrick.save()
                                # handle list of backuped models 
                                ls_backupedmodel.append( s_keymodel )
            self.stdout.write( "ok "+s_annotmodel )     
