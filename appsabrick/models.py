from django.db import models

# mport from python 
import datetime


# constants 
# sys admin reagent table 
REAGENTRECORD = (
    ("app0antibody","AntibodyPrimaryDetailLevel","antibody_primary","Antibody Primary"),
    ("app0antibody","AntibodySecondaryDetailLevel","antibody_secondary","Antibody Secondary"),
    ("app0compound","CompoundDetailLevel","compound","Compound"),
    ("app0protein","ProteinDetailLevel","protein","Protein"),
    ("app0protein","ProteinSetDetailLevel","proteinset","Protein Set"),
    #("app0rnai","RnaiDetailLevel","rnai","RNAi"),
) 

# reagent type choice used for reagent table
lts_REAGENTTYPE = []
for ts_record in REAGENTRECORD: 
    ts_reagenttype = (ts_record[-2], ts_record[-1])
    lts_REAGENTTYPE.append( ts_reagenttype )

REAGENTTYPE = tuple( lts_REAGENTTYPE )


# sys admin annot table 
ANNOTMODELRECORD = (
    # person (sampel, protocol, (assay), study, inversiation)
    ("app0person", "Person", (), 10),
    # publication (sample, study, investigation)
    ("app0publication", "Publication", (), 100),

    # protocol (assay)
    ("app0protocol", "ProtocolBasicLevel", (), 100),
    ("app0protocol", "ProtocolDetailLevel", ("ProtocolBasicLevel",), 101),
    ("app0protocol", "ProtocolComponentBasicLevel", (), 100),
    ("app0protocol", "ProtocolComponentDetailLevel", ("ProtocolComponentBasicLevel","ProtocolBasicLevel"), 101),
    ("app0protocol", "ProtocolRef", ("ProtocolDetailLevel","Person",), 102),
    ("app0protocol", "ResultFile", (), 102),
    ("app0protocol", "ResultUrl", (), 102),
    ("app0protocol", "ResultProtocolRef", ("ProtocolRef","ResultFile","ResultUrl"), 103),

    # sample 
    ("app0sample", "SampleBasicLevel", ("Publication",), 100),
    ("app0sample", "SampleDetailLevel", ("SampleBasicLevel",), 101),
    ("app0sample", "SampleLabLevel", ("SampleDetailLevel","Person",), 102),
    ("app0sample", "SampleSingle", ("SampleLabLevel",), 103),
    ("app0sample", "SampleSet", (), 100),

    # antibody (reagent)
    ("app0antibody", "AntibodyPrimaryBasicLevel", (), 100),
    ("app0antibody", "AntibodyPrimaryDetailLevel", ("AntibodyPrimaryBasicLevel",), 101),
    ("app0antibody", "AntibodySecondaryBasicLevel", (), 100),
    ("app0antibody", "AntibodySecondaryDetailLevel", ("AntibodySecondaryBasicLevel",), 101),
    # compoundy (reagent)
    ("app0compound", "CompoundBasicLevel", (), 100),
    ("app0compound", "CompoundDetailLevel", ("CompoundBasicLevel",), 101),
    # protein (reagent)
    ("app0protein", "ProteinBasicLevel", (), 200),
    ("app0protein", "ProteinDetailLevel", ("ProteinBasicLevel","SampleBasicLevel",), 201),
    ("app0protein", "ProteinSetProteinDetailLevel", ("ProteinBasicLevel","SampleBasicLevel",), 201),
    ("app0protein", "ProteinSetDetailLevel", ("ProteinSetProteinDetailLevel",), 202),

    # factor (reagent)
    ("app0factor", "FactorSingle", ("Reagent",), 300),
    ("app0factor", "FactorSet", ("Reagent",), 300),
    # parameter (reagent)
    ("app0parameter", "ParameterSingle", ("Reagent",), 300),
    ("app0parameter", "ParameterSet", ("Reagent",), 300),

    # study 1000
    ("app0study", "Study", (), 1000),
    ("app0study", "ZoomStudyPublication", ("Study","Publication",), 1001),
    ("app0study", "ZoomStudyPerson", ("Study","Person",), 1001),

    # investigation 2000
    ("app0investigation", "Investigation", (), 2000),
    ("app0investigation", "ZoomInvestigationPublication", ("Investigation","Publication",), 2001),
    ("app0investigation", "ZoomInvestigationPerson", ("Investigation","Person",), 2001),
    ("app0investigation", "ZoomInvestigationStudy", ("Investigation","Study",), 2001),

    # assays appsa_ 3000
    # mema 
    ("appas_mema", "SourcePlateLayout", (), 3000),
    ("appas_mema", "SourcePlateLayoutBarecodeLevel", ("SourcePlateLayout","FactorSet","ProtocolRef",), 3001),
    ("appas_mema", "AssayMema", ("SourcePlateBarecodeLevel","SampleSingle","FactorSet","ParameterSet","ProtocolRef",), 3002),
    ("appas_mema", "ZoomStudyAssayMema", ("AssayMema","Study",), 3003),
    ("appas_mema", "ZoomResultProtocolRefSet", ("AssayMema","ResultProtocolRef",), 3003),
    # rnasq
    #("", "", (), )
    #("", "", (), )
)


# Create your models here.
# reagent type name entities ( its basic is the detail level)
class ReagentManager( models.Manager ):
    def get_by_natural_key( self, reagent_type, reagent_name ):
        return self.get( reagent_type=reagent_type, reagent_name=reagent_name )

class Reagent( models.Model ):
    objects = ReagentManager() 
    reagent_type = models.SlugField( choices=REAGENTTYPE, help_text="Annot reagent type." )
    reagent_name = models.CharField( max_length=255, help_text="Detailed reagent name." )  # on detail level
    up_to_date = models.NullBooleanField( default=None )
    def __str__( self ):
        s_out= self.reagent_type+"|"+self.reagent_name
        return( s_out )
    def natural_key( self ):
        return ( self.reagent_type, self.reagent_name ) 

    class Meta : 
        unique_together = (("reagent_type","reagent_name"),)
        ordering = ["reagent_type","reagent_name"]


# sys admin reagent table
class SysAdminReagent( models.Model ): 
    app = models.SlugField( )
    reagent_model = models.SlugField( unique=True,  )
    reagent_type = models.SlugField( primary_key=True, )
    reagent_aliquot_count = models.PositiveSmallIntegerField( default=None, null=True, blank=True )
    reagent_last_vocabulary_pull = models.DateTimeField( default=datetime.datetime( year=1955, month=11, day=5 )) 
    def __str__( self ):
        return( self.reagent_model )

    class Meta:
        verbose_name_plural = "sys admin reagents" 
        ordering = ["reagent_type"]


# sys admin annot table
class SysAdminBrick( models.Model ): 
    app = models.SlugField( )
    model = models.SlugField( primary_key=True )
    model_ancestor = models.SlugField( max_length=255, default="", blank=True )
    backup_hierarchy_level = models.PositiveSmallIntegerField( default=None, null=True, blank=True ) 
    latest_backup = models.DateTimeField( default=datetime.datetime( year=1955, month=11, day=5 ) ) 
    def __str__( self ):
        return( self.model )

    class Meta:
        verbose_name_plural = "sys admin bricks" 
        ordering = ["app","model"]
