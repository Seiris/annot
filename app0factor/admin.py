from django.contrib import admin
from app0factor.models import FactorSingle, FactorSet

# Register your models here.

# factor 
#admin.site.register( FactorSingle )
class FactorSingleAdmin(  admin.ModelAdmin ):
    list_display = ("factor","reagent_aliquot_serial","reagent_concentration_unit","reagent_concentration_value")
    search_fields = ("factor","reagent_aliquot_serial","reagent_concentration_unit","reagent_concentration_value")
admin.site.register( FactorSingle, FactorSingleAdmin )


# factor set
#admin.site.register( FactorSet )
class FactorSetAdmin( admin.ModelAdmin ):
    list_display = ("factorset_name",)
    search_fields = ("factorset_name","factor")
    filter_horizontal = ("factor",)
admin.site.register( FactorSet, FactorSetAdmin )
