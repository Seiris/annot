from django.db import models
# ontologies
from apponunit_bioontology.models import Unit
from apponfactorsetname_own.models import FactorSetName
# bridges
from appsabrick.models import Reagent


# Create your models here.
# factor unit value
class FactorSingleManager( models.Manager ):
    def get_by_natural_key( self, factor__reagent_type, factor__reagent_name, reagent_aliquot_serial, reagent_concentration_unit, reagent_concentration_value ):
        return self.get( factor__reagent_type=factor__reagent_type, factor__reagent_name=factor__reagent_name, reagent_aliquot_serial=reagent_aliquot_serial, reagent_concentration_unit=reagent_concentration_unit, reagent_concentration_value=reagent_concentration_value )

class FactorSingle( models.Model ): 
    objects = FactorSingleManager()
    factor = models.ForeignKey( Reagent, help_text="Reagent becomming factor, a perturbator." )
    reagent_aliquot_serial = models.SlugField( max_length=255, default="not_yet_specified", verbose_name="Lab level aliquot identification", help_text="Write down the aliquot identifier. Only alphanumeric keys and underscore allowed." )
    reagent_concentration_unit = models.ForeignKey( Unit, default="not_yet_specified", help_text="Value unit." )
    reagent_concentration_value = models.FloatField( default=None, help_text="Value.", null=True, blank=True )
    def __str__( self ):
        s_out = self.factor.reagent_type+'|'+self.factor.reagent_name+'-'+self.reagent_aliquot_serial+'|'+self.reagent_concentration_unit.annot_id+'-'+str(self.reagent_concentration_value)
        return( s_out)
    def natural_key( self ): 
        return( self.factor.natural_key()+(self.reagent_aliquot_serial, self.reagent_concentration_unit.pk, self.reagent_concentration_value,) )
    natural_key.dependencies = ["appsabrick.Reagent","apponunit_bioontology.Unit"]

    class Meta: 
        unique_together = (("factor", "reagent_aliquot_serial", "reagent_concentration_unit", "reagent_concentration_value"),)
        ordering = ["factor","reagent_aliquot_serial","reagent_concentration_unit","reagent_concentration_value"]


# factor set
# factor set track concentration and aliquote ( barecode ) in assay separartely e.g. in mema layoufile  
class FactorSet( models.Model ):
    factorset_name = models.ForeignKey( FactorSetName, primary_key=True, default="not_yet_specified", help_text="Set a factorset name." )
    factor = models.ManyToManyField( Reagent, help_text="Choose reagents which are becomme factors, perturbators." )
    def __str__( self ):
        return( self.factorset_name.annot_id )

    class Meta:
        ordering =["factorset_name"]
